import React from 'react';
import { Alert, useEffect } from 'react-native';
import { Routes } from 'configs';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { Provider } from 'react-redux';
import store from 'configs/redux/store';
import FlashMessage from 'react-native-flash-message';
import codePush from 'react-native-code-push';
import Toast from 'react-native-toast-message';
import messaging from '@react-native-firebase/messaging';
import ScreenOrientation, {
  PORTRAIT,
  LANDSCAPE,
} from 'react-native-orientation-locker/ScreenOrientation';
import { showMessage, hideMessage } from 'react-native-flash-message';

let codePushOptions = {
  checkFrequency: codePush.CheckFrequency.ON_APP_RESUME,
  installMode: codePush.InstallMode.IMMEDIATE,
};
class App extends React.Component {
  componentDidMount() {
    messaging().onNotificationOpenedApp((remoteMessage) => {
      console.log(
        'Notification caused app to open from background state:',
        remoteMessage.notification,
      );
    });

    // Check whether an initial notification is available
    messaging()
      .getInitialNotification()
      .then((remoteMessage) => {
        if (remoteMessage) {
          console.log(
            'Notification caused app to open from quit state:',
            remoteMessage.notification,
          );
        }
      });
  }

  componentWillUnmount() {
    messaging().onMessage(async (remoteMessage) => {
      console.log(JSON.stringify(remoteMessage));
      Toast.show({
        text1: 'New Suscriber for you, check your notification',
        position: 'bottom',
        type: 'success',
      });
      console.log('A new FCM message arrived!', JSON.stringify(remoteMessage));
    });
  }
  render() {
    return (
      <Provider store={store}>
        <SafeAreaProvider>
          <ScreenOrientation
            orientation={PORTRAIT}
            onChange={(orientation) => console.log('onChange', orientation)}
            onDeviceChange={(orientation) =>
              console.log('onDeviceChange', orientation)
            }
          />
          <Routes />
          <FlashMessage position="top" autoHide={false} />
          <Toast ref={(ref) => Toast.setRef(ref)} />
        </SafeAreaProvider>
      </Provider>
    );
  }
}

export default codePush(codePushOptions)(App);
