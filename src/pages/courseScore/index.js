import {
  React,
  useEffect,
  useState,
  connect,
  View,
  SafeAreaView,
  StatusBar,
  TouchableOpacity,
  ImageBackground,
} from 'libraries';
import { Text, Button } from 'components';
import styles from './style';
import { appString, Color, scale } from 'utils';
import { fetchMyScore } from 'configs/redux/reducers/myCourse/myCourseActions';
import IMG from 'assets/images';
import LottieView from 'lottie-react-native';

const CourseScorePage = (props) => {
  const {
    scoreData,
    navigation,
    isLoading,
    dispatchCourseScore,
    route,
  } = props;

  const { courseId } = route.params;

  useEffect(() => {
    dispatchCourseScore({
      paramsId: courseId,
    });
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar
        translsucent
        backgroundColor="#2D8989"
        barStyle={'dark-content'}
      />

      <View style={styles.container}>
        {/* <ImageBackground source={IMG.certificateBackground} style={{flex:1}}> */}
        <View style={styles.filler} />
        <View style={{ marginHorizontal: scale(20) }}>
          <Text style={styles.header} numberOfLines={1}>
            Great Job!
          </Text>
          <Text style={styles.subheader} numberOfLines={1}>
            You've earned your certificate
          </Text>
        </View>
        <View style={{alignItems:'center', marginTop: scale(10)}}>
        <LottieView
            style={{ height: scale(90) }}
            source={require('../../assets/animasi/medals.json')}
            autoPlay
            loop
          />
        </View>
        <TouchableOpacity disabled={true} style={styles.touchableArea}>
          <View style={styles.detailContainer}>
            <View>
              <View style={{ marginBottom: scale(20) }}>
                <Text style={styles.title} numberOfLines={1}>
                  CERTIFICATE OF COMPLETION
                </Text>
                <Text style={styles.subtitle} numberOfLines={1}>
                  Awarded to
                </Text>
                <Text style={styles.name} numberOfLines={1}>
                  {scoreData?.user?.fullname}
                </Text>
              </View>
              <View style={{ marginBottom: scale(20) }}>
                <Text style={styles.subtitle} numberOfLines={2}>
                  In Recognition for Completing
                </Text>
                <Text style={styles.title} numberOfLines={1}>
                  {scoreData?.courseDetail?.title}
                </Text>
              </View>
              {scoreData?.mark ? (
                <View style={{ marginBottom: scale(20) }}>
                <Text style={styles.subtitle} numberOfLines={2}>
                  Score
                </Text>
                <Text style={styles.title} numberOfLines={1}>
                  {scoreData?.mark}
                </Text>
              </View>
              ) : null}
              <View>
                <Text style={styles.subtitle} numberOfLines={2}>
                  Certified By
                </Text>
                <Text style={styles.title} numberOfLines={1}>
                  {scoreData?.courseDetail?.author?.fullname}
                </Text>
              </View>
            </View>
          </View>
        </TouchableOpacity>
        <View>
          <Button
            buttonStyle={{
              paddingHorizontal: scale(20),
              marginTop: scale(30),
              backgroundColor: '#f7f78f',
              borderRadius: scale(4),
              flex: 1,
              marginHorizontal: scale(20),
            }}
            textStyle={{
              fontSize: scale(20),
              fontWeight: 'bold',
              color: '#4b4b4b',
            }}
            title={'DONE'}
            onPress={() => navigation.goBack()}
          />
        </View>
        {/* </ImageBackground> */}
      </View>
    </SafeAreaView>
  );
};

function mapStateToProps(state) {
  return {
    //isi state dari reducer
    scoreData: state.myCourseStore.scoreData,
    isLoading: state.myCourseStore.isLoading,
    error: state.myCourseStore.error,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    //isi dispatch
    dispatchCourseScore: (payload) => dispatch(fetchMyScore(payload)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CourseScorePage);
