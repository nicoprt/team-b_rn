import { StyleSheet } from 'libraries';
import { StatusBar } from 'react-native';
import { Color, scale, shadow, verticalScale } from 'utils';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Color.backgroundColor,
  },
  safearea: {
    flex: 1,
    flexGrow: 1,
  },
  title: {
    backgroundColor: Color.primaryColor,
    padding: scale(10),
  },
  titleText: {
    fontSize: scale(16),
    fontWeight: 'bold',
    color: 'white',
  },
  header: {
    marginTop: StatusBar.currentHeight,
    padding: scale(5),
  },
  scrollview: {
    flex: 1,
  },
  contentContainer: {
    flex: 1,
    justifyContent: 'space-between',
  },
  content: {
    flex: 1,
    flexGrow: 1,
    padding: scale(10),
    // backgroundColor: 'blue',
  },
  emptyExam: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  emptyExamLabel: {
    fontSize: scale(16),
    fontWeight: '400',
  },
  btnStyle: {
    backgroundColor: Color.primaryColor,
  },
  btnStyleLabel: {
    fontSize: scale(16),
    fontWeight: '400',
    color: 'white',
  },
  bottomSheetContent: {
    backgroundColor: 'white',
    height: '100%',
    padding: scale(10),
  },
  shadowContainer: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: '#000',
    zIndex: 5,
  },
  questionTypeContainer: {
    flexDirection: 'row',
  },
  questionType: {
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: Color.primaryColor,
    borderRadius: scale(5),
    marginRight: scale(5),
    padding: scale(5),
    justifyContent: 'center',
    alignItems: 'center',
    width: scale(100),
  },
  questionTypeSelected: {
    backgroundColor: Color.primaryColor,
  },
  questionTypeLabel: {
    fontSize: scale(14),
    fontWeight: '400',
    color: 'black',
  },
  questionTypeLabelSelected: {
    color: 'white',
  },
  textInput: {
    backgroundColor: 'white',
    borderRadius: scale(5),
    padding: scale(10),
    marginBottom: scale(5),
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  questionFormContainer: {
    padding: scale(5),
  },
  choicesContainer: {
    flex: 1,
  },
  choices: {
    // flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textInputChoices: {
    flex: 1,
    marginHorizontal: scale(5),
  },
  optionAnswer: {
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: 'green',
    justifyContent: 'center',
    alignItems: 'center',
    padding: scale(5),
    borderRadius: scale(5),
    marginRight: scale(5),
    ...shadow,
  },
  deleteOptions: {
    backgroundColor: 'red',
    justifyContent: 'center',
    alignItems: 'center',
    padding: scale(5),
    borderRadius: scale(5),
    ...shadow,
  },
  bottomSheetSrollView: {
    flex: 1,
  },
  bottomSheetKeyboardAvoiding: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  formLabel: {
    fontSize: scale(16),
  },
});

export default styles;
