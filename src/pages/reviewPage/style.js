import { StyleSheet, StatusBar } from 'react-native';
import { Color, scale, METRICS, FONTS } from 'utils';
const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight,
    backgroundColor: Color.backgroundColor,
  },
  scrollWrapper: { alignItems: 'center' },
  title: {
    fontSize: scale(30),
    textAlign: 'center',
    marginTop: scale(30),
  },
  imageContainer: {
    width: scale(80),
    height: scale(80),
    marginTop: scale(20),
    marginBottom: scale(10),
  },
  image: {
    width: scale(80),
    height: scale(80),
  },
  messageRating: {
    height: scale(200),
    borderColor: 'gray',
    backgroundColor: 'white',
    width: METRICS.screen.width * 0.9,
    borderRadius: scale(10),
    textAlignVertical: 'top',
    fontFamily: FONTS.brandonGrotesque,
    fontSize: scale(20),
  },
  textButton: {
    color: 'white',
  },
  modalContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0,0,0,0.4)',
  },
  modalContent: {
    backgroundColor: 'white',
    borderRadius: scale(10),
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
    position: 'relative',
    minHeight: scale(120),
    width: METRICS.window.width * 0.6,
  },
  successImage: {
    height: scale(100),
    aspectRatio: 4 / 3,
    marginTop: scale(20),
    marginBottom: scale(15),
    resizeMode: 'contain',
  },
  successTitle: { fontSize: scale(20), marginBottom: scale(10) },
  successSubtitle: {
    fontSize: scale(15),
    textAlign: 'center',
    marginBottom: scale(10),
  },
  textButtonHome: {
    color: 'white',
    fontSize: scale(15),
  },
  pressStyle: { flex: 1 },
});

export default styles;
