import { Button, ExamCard, PageHeader, Text } from 'components';
import { connect, React, useIsFocused, View } from 'libraries';
import { ScrollView } from 'react-native';
import { SafeAreaView } from 'react-native';
import { scale, verticalScale } from 'utils';
import styles from './style';
import EmptyExamSVG from 'assets/images/emptyExams.svg';
import { getExams } from 'configs/redux/reducers/myCourse/myCourseActions';
import { useEffect } from 'react';

const ExamPage = ({ route, navigation, ...props }) => {
  // properties data
  const { course } = route.params;
  const { courseExams } = props;
  const { dispatchGetExam } = props;
  const isFocused = useIsFocused();

  console.log(course);

  // function
  const handlerBackBtn = () => navigation.goBack();
  const handlerCreateBtn = () =>
    navigation.navigate('ExamCreatePage', {
      course: course,
    });

  // component
  const EmptyExamComponent = () => {
    return (
      <View style={styles.emptyExam}>
        <EmptyExamSVG width={scale(250)} height={scale(250)} />
        <Text style={styles.emptyExamLabel}>Let's create your first exam</Text>
      </View>
    );
  };

  const ExamComponent = () => {
    return (
      <View style={styles.examContainer}>
        {courseExams.map((item, index) => {
          return (
            <View key={item.questionId} style={styles.examCardContainer}>
              <ExamCard item={item} />
            </View>
          );
        })}
      </View>
    );
  };

  // useEffects
  useEffect(() => {
    const payload = {
      paramsId: course.id,
    };
    dispatchGetExam(payload);
  }, [course.id, dispatchGetExam]);

  useEffect(() => {
    //Update the state you want to be updated
    const payload = {
      paramsId: course.id,
    };
    dispatchGetExam(payload);
  }, [course.id, dispatchGetExam, isFocused]);

  return (
    <>
      <SafeAreaView style={styles.container}>
        <View style={styles.header}>
          <PageHeader title={course.title} backAction={handlerBackBtn} />
        </View>
        <View style={styles.title}>
          <Text style={styles.titleText}>Exam Questions</Text>
        </View>
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentInset={{ bottom: verticalScale(100) }}
          contentContainerStyle={styles.contentContainer}
          // bounces={false}
          style={styles.content}>
          {courseExams.length === 0 ? (
            <EmptyExamComponent />
          ) : (
            <ExamComponent />
          )}
        </ScrollView>
      </SafeAreaView>
      {course.isPublished === false &&
        (course.subscriberCount === undefined ||
          course.subscriberCount === '0') && (
          <View style={styles.bottomActionContainer}>
            <SafeAreaView>
              <View>
                <Button
                  title={'Add Exam Question'}
                  buttonStyle={styles.btnStyle}
                  textStyle={styles.btnStyleLabel}
                  onPress={handlerCreateBtn}
                />
              </View>
            </SafeAreaView>
          </View>
        )}
    </>
  );
};

function mapStateToProps(state) {
  return {
    //isi state dari reducer
    courseExams: state.myCourseStore.courseExams,
    isLoading: state.myCourseStore.isLoading,
    error: state.myCourseStore.error,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    //isi dispatch
    dispatchGetExam: (payload) => dispatch(getExams(payload)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ExamPage);
