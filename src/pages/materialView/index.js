import { React, useEffect, useState, connect, View, EvaIcon } from 'libraries';
import { Text } from 'components';
import styles from './style';
import { Color, scale } from 'utils';
import VideoPlayer from 'react-native-video-controls';
import { useRef } from 'react';
import { base_url } from 'configs/api/url';
import Pdf from 'react-native-pdf';
import TrackPlayer from 'react-native-track-player';
import { BackHandler, Image, Pressable } from 'react-native';
import {
  useTrackPlayerProgress,
  useTrackPlayerEvents,
} from 'react-native-track-player/lib/hooks';
import Slider from '@react-native-community/slider';
import { TrackPlayerEvents, STATE_PLAYING } from 'react-native-track-player';
import { StatusBar } from 'react-native';
import { SafeAreaView } from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {
  flagCompleteMaterial,
  getMaterialCourse,
} from 'configs/redux/reducers/course/courseActions';
import Toast from 'react-native-toast-message';

const MaterialView = (props) => {
  const { navigation, route, dispatchReadFlag } = props;
  const { data, thumb, courseId } = route.params;
  const videoReff = useRef(null);
  const [isTrackPlayerInit, setIsTrackPlayerInit] = useState(false);
  const [isPlaying, setIsPlaying] = useState(false);
  const [sliderValue, setSliderValue] = useState(0);
  const [isSeeking, setIsSeeking] = useState(false);
  const { position, duration } = useTrackPlayerProgress(250);
  const [currPage, setCurrPage] = useState({
    curr: null,
    total: null,
  });

  const urlFiles = `${base_url}/files/course/material/${data.path}`;

  const flagComplete = async () => {
    try {
      dispatchReadFlag(
        {
          paramsId: data.id,
        },
        courseId,
      );
    } catch (e) {}
  };

  const timeFormat = (given_seconds) => {
    let minutes = Math.floor((given_seconds % 3600) / 60);
    let seconds = Math.floor((given_seconds % 3600) % 60);
    let timeString =
      minutes.toString().padStart(2, '0') +
      ':' +
      seconds.toString().padStart(2, '0');
    return timeString;
  };

  useTrackPlayerEvents([TrackPlayerEvents.PLAYBACK_STATE], (event) => {
    if (event.state === STATE_PLAYING) {
      setIsPlaying(true);
    } else {
      setIsPlaying(false);
    }
  });

  useEffect(() => {
    if (data.type === 'audio') {
      const startPlayer = async () => {
        let isInit = await trackPlayerInit();
        setIsTrackPlayerInit(isInit);
      };
      startPlayer();
      const backAction = async () => {
        await TrackPlayer.stop();
        navigation.pop();
        return true;
      };

      const backHandler = BackHandler.addEventListener(
        'hardwareBackPress',
        backAction,
      );

      return () => backHandler.remove();
    }
  }, []);

  useEffect(() => {
    if (!isSeeking && position && duration) {
      setSliderValue(position / duration);
      if (
        timeFormat(position) === timeFormat(duration) ||
        position === duration - 1
      ) {
        //audio complete
        Toast.show({
          text1: 'Thank you, you have been complete this audio material',
          position: 'bottom',
          type: 'success',
        });
        flagComplete();
      }
    }
  }, [position, duration]);

  //function to initialize the Track Player
  const trackPlayerInit = async () => {
    await TrackPlayer.setupPlayer();
    TrackPlayer.updateOptions({
      stopWithApp: true,
      capabilities: [
        TrackPlayer.CAPABILITY_PLAY,
        TrackPlayer.CAPABILITY_PAUSE,
        TrackPlayer.CAPABILITY_JUMP_FORWARD,
        TrackPlayer.CAPABILITY_JUMP_BACKWARD,
      ],
    });
    await TrackPlayer.add({
      id: '1',
      url: urlFiles,
      type: 'default',
      title: data.title,
      album: 'Online Training',
      artist: 'Online Training',
      artwork: thumb,
    });
    await TrackPlayer.play();
    return true;
  };

  const audioForward = async () => {
    if (position < duration) {
      await TrackPlayer.play();
      await TrackPlayer.seekTo(position + 10);
    }
    setIsSeeking(false);
  };

  const audioBackward = async () => {
    if (position > 0) {
      await TrackPlayer.play();
      await TrackPlayer.seekTo(position - 10);
    }
    setIsSeeking(false);
  };

  const onButtonPressed = () => {
    if (!isPlaying) {
      TrackPlayer.play();
      setIsPlaying(true);
    } else {
      TrackPlayer.pause();
      setIsPlaying(false);
    }
  };

  const slidingStarted = () => {
    setIsSeeking(true);
  };

  const slidingCompleted = async (value) => {
    await TrackPlayer.seekTo(value * duration);
    setSliderValue(value);
    setIsSeeking(false);
  };

  const onChangeSliding = async (value) => {
    setIsSeeking(false);
    setSliderValue(value);
    await TrackPlayer.seekTo(value * duration);
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.navbar}>
        <Pressable
          onPress={async () => {
            if (data.type === 'audio') {
              await TrackPlayer.stop();
            }
            navigation.pop();
          }}>
          <View style={styles.backButton}>
            <EvaIcon
              name={'arrow-ios-back-outline'}
              width={scale(20)}
              height={scale(20)}
              fill={'white'}
            />
          </View>
        </Pressable>

        <Text bold style={styles.titleMaterial}>
          {data.title.toLowerCase()}
        </Text>
        {data.type === 'pdf' ? (
          <Text style={{ fontSize: scale(20) }}>
            {currPage.curr}/{currPage.total}
          </Text>
        ) : (
          <View />
        )}
      </View>

      <StatusBar
        backgroundColor={Color.backgroundColor}
        barStyle={'dark-content'}
      />
      {data.type === 'video' ? (
        <View style={styles.videoWrapper}>
          <VideoPlayer
            source={{ uri: urlFiles }} // Can be a URL or a local file.
            ref={videoReff} // Store reference
            controls={true}
            onEnd={() => {
              Toast.show({
                text1: 'Thank you, you have been complete this video material',
                position: 'bottom',
                type: 'success',
              });
              flagComplete();
            }}
            fullscreenAutorotate={true}
          />
        </View>
      ) : data.type === 'pdf' ? (
        <View style={styles.pdfContainer}>
          <Pdf
            source={{ uri: urlFiles, cache: true }}
            onPageChanged={(page, numberOfPages) => {
              setCurrPage({
                ...currPage,
                curr: page,
                total: numberOfPages,
              });

              if (page === numberOfPages) {
                Toast.show({
                  text1: 'Thank you, you have been complete this pdf material',
                  position: 'bottom',
                  type: 'success',
                });
                flagComplete();
              }
            }}
            onError={(error) => {
              console.log(error);
            }}
            onPressLink={(uri) => {
              console.log(`Link presse: ${uri}`);
            }}
            style={styles.pdf}
            enablePaging={true}
          />
        </View>
      ) : (
        <View style={styles.mainContainer}>
          <View style={styles.music_logo_view}>
            <Image
              source={{
                uri: thumb,
              }}
              resizeMode="contain"
              style={styles.albumImage}
            />
          </View>

          <View style={styles.name_of_song_View}>
            <Text numberOfLines={1} bold style={styles.name_of_song_Text1}>
              {data.title}
            </Text>
            <Text numberOfLines={2} style={styles.name_of_song_Text2}>
              {data.desc}
            </Text>
          </View>

          <View style={styles.slider_view}>
            <Text style={styles.slider_time}>{timeFormat(position)}</Text>
            <Slider
              style={styles.slider_style}
              minimumValue={0}
              maximumValue={1}
              value={sliderValue}
              minimumTrackTintColor={Color.primaryColor}
              maximumTrackTintColor="#d3d3d3"
              onSlidingStart={slidingStarted}
              onSlidingComplete={slidingCompleted}
              thumbTintColor={Color.primaryColor}
              onValueChange={onChangeSliding}
            />
            <Text style={styles.slider_time}>{timeFormat(duration)}</Text>
          </View>
          <View style={styles.functions_view}>
            <Pressable onPress={audioBackward}>
              <Entypo
                name="controller-fast-backward"
                size={scale(35)}
                color={Color.primaryColor}
              />
            </Pressable>
            <Pressable onPress={onButtonPressed} style={styles.musicControl}>
              <AntDesign
                name={isPlaying ? 'pausecircle' : 'playcircleo'}
                size={scale(60)}
                color={Color.primaryColor}
              />
            </Pressable>
            <Pressable onPress={audioForward}>
              <Entypo
                name="controller-fast-forward"
                size={scale(35)}
                color={Color.primaryColor}
              />
            </Pressable>
          </View>
        </View>
      )}
      <Toast ref={(ref) => Toast.setRef(ref)} />
    </SafeAreaView>
  );
};

function mapStateToProps(state) {
  return {};
}

function mapDispatchToProps(dispatch) {
  return {
    dispatchGetMaterial: (payload) => dispatch(getMaterialCourse(payload)),
    dispatchReadFlag: (payload, courseId) =>
      dispatch(flagCompleteMaterial(payload, courseId)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(MaterialView);
