import {
  React,
  useEffect,
  useState,
  connect,
  View,
  SafeAreaView,
  StatusBar,
  Image,
} from 'libraries';
import { Button, Text } from 'components';
import styles from './style';
import { Color, scale } from 'utils';
import IMG from 'assets/images';
import SelectBox from 'react-native-multi-selectbox';
import { xorBy } from 'lodash';
import {
  getCourseCategory,
  getMyPreferenceCategory,
} from 'configs/redux/reducers/course/courseActions';
import API from 'configs/api';
import Toast from 'react-native-toast-message';
import { expiredAction } from 'configs/redux/reducers/auth/authActions';

const PreferredCategoryPage = (props) => {
  const {
    navigation,
    dispatchGetCategory,
    course_category,
    dispatchGetMyPreferenceCategory,
    dispatchExpired,
  } = props;

  const [selectedTeams, setSelectedTeams] = useState([]);
  const [categoryData, setCategoryData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  function onMultiChange() {
    return (item) => setSelectedTeams(xorBy(selectedTeams, [item], 'id'));
  }

  useEffect(() => {
    dispatchGetCategory();
  }, []);

  useEffect(() => {
    let newData = [];
    if (course_category.data.length > 0) {
      course_category.data.forEach((item, index) => {
        newData.push({
          item: item.name,
          id: item.id,
        });
      });
      setCategoryData(newData);
    }
  }, [course_category]);

  const handleSubmit = async () => {
    try {
      setIsLoading(true);
      const idSelected = [];
      selectedTeams.forEach((item, index) => {
        idSelected.push(item.id);
      });
      const res = await API.postUserPreference({
        body: {
          categoryIds: idSelected,
        },
      });
      if (res) {
        setIsLoading(false);
        dispatchGetMyPreferenceCategory();
        navigation.navigate('BottomTab');
      }
    } catch (e) {
      if (e.status === 401) {
        dispatchExpired();
      }
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar backgroundColor={'#F5FAFA'} barStyle={'dark-content'} />
      <View style={styles.imageContainer}>
        <Image source={IMG.onlineTraining} style={styles.image} />

        <Text bold style={{ fontSize: scale(20) }}>
          Please select your Favorite Category
        </Text>
        <View style={styles.boxWrapper}>
          {categoryData.length > 0 ? (
            <SelectBox
              label=""
              options={categoryData}
              selectedValues={selectedTeams}
              onMultiSelect={onMultiChange()}
              onTapClose={onMultiChange()}
              isMulti
              multiOptionContainerStyle={{
                backgroundColor: Color.primaryColor,
              }}
              multiOptionsLabelStyle={{
                backgroundColor: Color.primaryColor,
              }}
            />
          ) : (
            <View />
          )}
        </View>
        <Button
          title={'Next'}
          buttonStyle={{
            backgroundColor: Color.primaryColor,
            borderRadius: scale(50),
            width: scale(200),
          }}
          textStyle={styles.buttonText}
          onPress={() => {
            if (selectedTeams.length > 0) {
              handleSubmit();
            } else {
              Toast.show({
                text1: 'You have to select at least 1 category!',
                position: 'bottom',
                type: 'error',
              });
            }
          }}
          isLoading={isLoading}
        />
      </View>
      <Toast ref={(ref) => Toast.setRef(ref)} />
    </SafeAreaView>
  );
};

function mapStateToProps(state) {
  return {
    course_category: state.courseStore.course_category,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatchGetCategory: () => dispatch(getCourseCategory()),
    dispatchGetMyPreferenceCategory: () => dispatch(getMyPreferenceCategory()),
    dispatchExpired: () => dispatch(expiredAction()),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PreferredCategoryPage);
