import {
  React,
  useEffect,
  connect,
  View,
  Image,
  InteractionManager,
  Pressable,
  ScrollView,
  StatusBar,
  RefreshControl,
} from 'libraries';
import {
  Text,
  CategoryCard,
  CardCourseVertical,
  CardCourseHorizontal,
} from 'components';
import styles from './style';
import { scale, METRICS, Color } from 'utils';
import { Icon } from 'react-native-eva-icons';
import {
  getCourseCategory,
  getNewCourse,
  getPopularCourse,
  getRecentCourse,
  getMostLikeCourse,
  getMyPreferenceCategory,
} from 'configs/redux/reducers/course/courseActions';
import { base_url } from 'configs/api/url';
import { getNotification } from 'configs/redux/reducers/profile/profileActions';
import EmptyCourse from 'assets/images/emptyCourse.svg';

const HomePage = (props) => {
  const {
    navigation,
    dispatchGetCategory,
    course_category,
    dispatchGetPopularCourse,
    popular_course,
    dispatchGetNewCourse,
    new_course,
    dispatchGetRecentCourse,
    recent_course,
    dispatchGetMostLikeCourse,
    most_like_course,
    dispatchGetMyPreferenceCategory,
    preference,
    username,
    profilePicture,
    notification,
    dispatchGetNotification,
  } = props;

  const payload = {
    params: {
      page: 1,
      pageSize: 10,
    },
  };
  const [refreshing, setRefreshing] = React.useState(false);
  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    dispatchGetPopularCourse(payload);
    dispatchGetNewCourse({
      params: {
        page: 1,
        pageSize: 3,
      },
    });
    dispatchGetRecentCourse(payload);
    dispatchGetMostLikeCourse(payload);
    dispatchGetNotification();
    setRefreshing(false);
  }, []);

  useEffect(() => {
    //untuk mastiin semua animasi transisi dari navigasi nggak terganggu
    const task = InteractionManager.runAfterInteractions(() => {
      dispatchGetCategory();
      dispatchGetPopularCourse(payload);
      dispatchGetNewCourse({
        params: {
          page: 1,
          pageSize: 3,
        },
      });
      dispatchGetRecentCourse(payload);
      dispatchGetMostLikeCourse(payload);
      dispatchGetMyPreferenceCategory();
      dispatchGetNotification();
    });

    return () => task.cancel();
  }, []);

  const navigateToAllCourse = (title, type, id) => {
    navigation.navigate('AllCoursePage', {
      title,
      type,
      id,
    });
  };

  const navigateToProfilePage = () => {
    navigation.navigate('ProfilePage');
  };

  const navigateToCourseDetail = (item) => {
    navigation.navigate('CourseDetailPage', {
      data: item,
    });
  };

  return (
    <View style={styles.container}>
      <StatusBar
        translucent
        backgroundColor={'#F5FAFA'}
        barStyle={'dark-content'}
      />

      <View style={styles.topbar}>
        <Pressable onPress={() => navigation.navigate('NotificationPage')}>
          <View style={styles.notifContainer}>
            <View style={styles.notification}>
              <Icon
                name="bell"
                width={styles.icon.width}
                height={styles.icon.height}
                fill={styles.icon.backgroundColor}
              />
              {notification.isNew ? <View style={styles.notifRed} /> : <View />}
            </View>
          </View>
        </Pressable>
        <Pressable
          onPress={() => {
            navigateToProfilePage();
          }}>
          <View style={styles.userContainer}>
            <View style={styles.userWrapper}>
              <Text bold style={styles.userText}>
                {username}
              </Text>
            </View>
            <Image
              source={{
                uri:
                  profilePicture !== null
                    ? `${base_url}/files/user/thumb/${profilePicture}`
                    : 'https://ppid.sulselprov.go.id/assets/front/img/kadis/avatar.png',
              }}
              style={styles.avatar}
            />
          </View>
        </Pressable>
      </View>
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={{ marginHorizontal: scale(-15) }}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }>
        <View style={styles.titleContainer}>
          <View style={{ marginTop: scale(5) }}>
            <Text bold style={{ fontSize: scale(30) }}>
              Find
            </Text>
            <Text medium style={{ fontSize: scale(20) }}>
              <Text medium style={styles.title}>
                {'Course '}
              </Text>
              you want to learn
            </Text>
          </View>
          <View style={styles.categoryWrapper}>
            <ScrollView horizontal showsHorizontalScrollIndicator={false}>
              {course_category.data.length > 0 ? (
                course_category.data.map((item, index) => {
                  return (
                    <CategoryCard
                      key={index}
                      categoryName={item.name}
                      iconName={item.icon}
                      onPress={() => {
                        navigateToAllCourse(item.name, 'category', item.id);
                      }}
                    />
                  );
                })
              ) : (
                <View />
              )}
            </ScrollView>
          </View>
          {course_category.data.length > 0 &&
          popular_course.data.length === 0 &&
          new_course.data.length === 0 &&
          most_like_course.data.length === 0 ? (
            <View style={styles.emptyCourseWrapper}>
              <EmptyCourse width={scale(250)} height={scale(250)} />
              <Text bold style={styles.emptyCourse}>
                Don't worry, high quality course will come to you soon. You can
                also create your own course
              </Text>
            </View>
          ) : (
            <View />
          )}
          {recent_course.data.length > 0 ? (
            <View>
              <View style={{ marginTop: scale(20), marginBottom: scale(10) }}>
                <Text
                  bold
                  style={{ fontSize: scale(17), letterSpacing: scale(-0.2) }}>
                  RECENT COURSE
                </Text>
                <View style={styles.greenLine} />
              </View>
              <View style={{ marginRight: scale(-15), marginTop: scale(5) }}>
                <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                  {recent_course.data.slice(0, 3).map((item, index) => {
                    return (
                      <View
                        key={index}
                        style={{ width: METRICS.screen.width * 0.9 }}>
                        <CardCourseHorizontal
                          desc={item.desc}
                          isLoved={item.isLoved}
                          isWishList={item.isWishlisted}
                          lovedCount={item.loveCount.toString()}
                          thumb={
                            item.picture !== null
                              ? `${base_url}/files/course/image/${item.picture}`
                              : 'https://akm-img-a-in.tosshub.com/indiatoday/images/story/201811/online-3412473_1920_1.jpeg'
                          }
                          title={item.title}
                          viewCount={item.viewCount.toString()}
                          onPress={() => {
                            navigateToCourseDetail(item);
                          }}
                        />
                      </View>
                    );
                  })}
                </ScrollView>
              </View>
            </View>
          ) : (
            <View />
          )}
          {popular_course.data.length > 0 ? (
            <View>
              <View style={{ marginTop: scale(20), marginBottom: scale(10) }}>
                <View style={styles.row}>
                  <Text
                    bold
                    style={{ fontSize: scale(17), letterSpacing: scale(-0.2) }}>
                    POPULAR COURSE
                  </Text>
                  <Pressable
                    onPress={() => {
                      navigateToAllCourse('POPULAR COURSE', 'popular', 0);
                    }}>
                    <Text medium style={styles.seemore}>
                      See More
                    </Text>
                  </Pressable>
                </View>
                <View style={styles.greenLine} />
              </View>
              <View style={{ marginRight: scale(-15) }}>
                <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                  {popular_course.data.map((item, index) => {
                    return (
                      <CardCourseVertical
                        key={index}
                        title={item.title}
                        desc={item.desc}
                        lovedCount={item.loveCount.toString()}
                        viewCount={item.viewCount.toString()}
                        imageUri={
                          item.picture !== null
                            ? `${base_url}/files/course/image/${item.picture}`
                            : 'https://akm-img-a-in.tosshub.com/indiatoday/images/story/201811/online-3412473_1920_1.jpeg'
                        }
                        isLoved={item.isLoved}
                        isWishList={item.isWishlisted}
                        onPress={() => {
                          navigateToCourseDetail(item);
                        }}
                      />
                    );
                  })}
                </ScrollView>
              </View>
            </View>
          ) : (
            <View />
          )}
          {new_course.data.length > 0 ? (
            <View>
              <View style={{ marginTop: scale(20), marginBottom: scale(10) }}>
                <View style={styles.row}>
                  <Text
                    bold
                    style={{ fontSize: scale(17), letterSpacing: scale(-0.2) }}>
                    NEW COURSE
                  </Text>
                  <Pressable
                    onPress={() => {
                      navigateToAllCourse('NEW COURSE', 'new', 0);
                    }}>
                    <Text medium style={styles.seemore}>
                      See More
                    </Text>
                  </Pressable>
                </View>
                <View style={styles.greenLine} />
              </View>
              <View style={{ marginTop: scale(5) }}>
                <ScrollView showsHorizontalScrollIndicator={false}>
                  {new_course.data.slice(0, 3).map((item, index) => {
                    return (
                      <CardCourseHorizontal
                        key={index}
                        title={item.title}
                        desc={item.desc}
                        lovedCount={item.loveCount.toString()}
                        viewCount={item.viewCount.toString()}
                        thumb={
                          item.picture !== null
                            ? `${base_url}/files/course/image/${item.picture}`
                            : 'https://akm-img-a-in.tosshub.com/indiatoday/images/story/201811/online-3412473_1920_1.jpeg'
                        }
                        isLoved={item.isLoved}
                        isWishList={item.isWishlisted}
                        onPress={() => {
                          navigateToCourseDetail(item);
                        }}
                      />
                    );
                  })}
                </ScrollView>
              </View>
            </View>
          ) : (
            <View />
          )}
          {preference.data.length > 0 &&
          !(
            popular_course.data.length === 0 &&
            new_course.data.length === 0 &&
            most_like_course.data.length === 0
          ) ? (
            <View>
              <View style={{ marginTop: scale(20), marginBottom: scale(10) }}>
                <View style={styles.row}>
                  <Text
                    bold
                    style={{ fontSize: scale(17), letterSpacing: scale(-0.2) }}>
                    MY PREFERENCE CATEGORY
                  </Text>
                </View>

                <View style={styles.greenLine} />
              </View>
              <View
                style={{
                  marginTop: scale(10),
                  marginRight: scale(-15),
                }}>
                <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                  {preference.data.map((item, index) => {
                    return (
                      <CategoryCard
                        key={index}
                        categoryName={item.courseCategory.name}
                        iconName={item.courseCategory.icon}
                        onPress={() => {
                          navigateToAllCourse(
                            item.courseCategory.name,
                            'category',
                            item.categoryId,
                          );
                        }}
                      />
                    );
                  })}
                </ScrollView>
              </View>
            </View>
          ) : (
            <View />
          )}
          {most_like_course.data.length > 0 ? (
            <View>
              <View style={{ marginTop: scale(20), marginBottom: scale(10) }}>
                <View style={styles.row}>
                  <Text
                    bold
                    style={{ fontSize: scale(17), letterSpacing: scale(-0.2) }}>
                    MOST LIKE COURSE
                  </Text>
                  <Pressable
                    onPress={() => {
                      navigateToAllCourse('MOST LIKE COURSE', 'mostLike', 0);
                    }}>
                    <Text medium style={styles.seemore}>
                      See More
                    </Text>
                  </Pressable>
                </View>
                <View style={styles.greenLine} />
              </View>
              <View style={{ marginTop: scale(5) }}>
                <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                  {most_like_course.data.slice(0, 10).map((item, index) => {
                    return (
                      <CardCourseVertical
                        key={index}
                        title={item.title}
                        desc={item.desc}
                        lovedCount={item.loveCount.toString()}
                        viewCount={item.viewCount.toString()}
                        imageUri={
                          item.picture !== null
                            ? `${base_url}/files/course/image/${item.picture}`
                            : 'https://akm-img-a-in.tosshub.com/indiatoday/images/story/201811/online-3412473_1920_1.jpeg'
                        }
                        isLoved={item.isLoved}
                        isWishList={item.isWishlisted}
                        onPress={() => {
                          navigateToCourseDetail(item);
                        }}
                      />
                    );
                  })}
                </ScrollView>
              </View>
            </View>
          ) : (
            <View />
          )}

          <View style={{ height: scale(60) }} />
        </View>
      </ScrollView>
    </View>
  );
};

function mapStateToProps(state) {
  return {
    course_category: state.courseStore.course_category,
    popular_course: state.courseStore.popular_course,
    new_course: state.courseStore.new_course,
    recent_course: state.courseStore.recent_course,
    most_like_course: state.courseStore.most_like_course,
    preference: state.courseStore.preference,
    username: state.profileStore.username,
    profilePicture: state.profileStore.profilePicture,
    notification: state.profileStore.notification,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatchGetCategory: () => dispatch(getCourseCategory()),
    dispatchGetPopularCourse: (payload) => dispatch(getPopularCourse(payload)),
    dispatchGetNewCourse: (payload) => dispatch(getNewCourse(payload)),
    dispatchGetRecentCourse: (payload) => dispatch(getRecentCourse(payload)),
    dispatchGetMostLikeCourse: (payload) =>
      dispatch(getMostLikeCourse(payload)),
    dispatchGetMyPreferenceCategory: () => dispatch(getMyPreferenceCategory()),
    dispatchGetNotification: () => dispatch(getNotification()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
