import { BackButton, Button, MaterialCard, Text, TextInput } from 'components';
import { base_url } from 'configs/api/url';
import {
  fetchMyCourse,
  getMaterials,
  postMateri,
  updateFileMateri,
  updateMateri,
} from 'configs/redux/reducers/myCourse/myCourseActions';
import {
  React,
  View,
  SafeAreaView,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Platform,
  Keyboard,
  ScrollView,
  useState,
  EvaIcon,
  TouchableOpacity,
  connect,
} from 'libraries';
import { useEffect, useRef } from 'react';
import { StatusBar } from 'react-native';
import DocumentPicker from 'react-native-document-picker';
import { launchImageLibrary } from 'react-native-image-picker';
import Video from 'react-native-video';
import VideoPlayer from 'react-native-video-controls';
import { Color } from 'utils';
import styles from './style';

const MaterialAdd = (props) => {
  const {
    navigation,
    route,
    isLoading,
    dispatchUpdateMaterial,
    dispatchUpdateFileMaterial,
    dispatchGetMyCourses,
    dispatchFetchMaterials,
    dispatchPostMaterial,
  } = props;
  const { course, material } = route.params;
  const [title, setTitle] = useState();
  const [desc, setDesc] = useState();
  const [materials, setMaterials] = useState([]);
  const playerRef = useRef(null);

  const selectFileHandler = async () => {
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.pdf],
      });

      const material = {
        uri: res.uri,
        type: res.type, // mime type
        name: res.name,
        size: res.size,
      };

      setMaterials([...materials, material]);
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  };

  const selectAudioHandler = async () => {
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.audio],
      });

      const material = {
        uri: res.uri,
        type: res.type, // mime type
        name: res.name,
        size: res.size,
      };

      console.log(material);

      setMaterials([...materials, material]);
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  };

  const selectVideoHandler = () => {
    const options = {
      mediaType: 'video',
      videoQuality: 'high',
    };

    launchImageLibrary(options, (response) => {
      console.log(response);
      const videoType = Platform.OS === 'ios' ? 'video/mov' : 'video/mp4';
      if (response.uri != undefined) {
        const tempVideo = response.uri.split('/');
        let videoName = tempVideo[tempVideo.length - 1];
        videoName = videoName.split('.').slice(0, -1).join('.');
        videoName = `${videoName}.${Platform.OS === 'ios' ? 'mov' : 'mp4'}`;
        videoName = `${
          title != ''
            ? title
            : response.fileName != undefined
            ? response.fileName
            : 'UnnamedFile'
        }.${Platform.OS === 'ios' ? 'mov' : 'mp4'}`;

        const material = {
          uri: response.uri,
          type: videoType, // mime type
          name: videoName,
          size: response.fileSize,
        };

        setMaterials([...materials, material]);
      }
    });
  };

  const removeFileHandler = (index) => {
    let temp = [...materials];
    temp.splice(index, 1);
    setMaterials(temp);
  };

  const addMaterialHandler = async () => {
    try {
      const form = new FormData();
      form.append('courseId', course.id);
      form.append('title', title);
      form.append('desc', desc);
      if (materials.length === 1) {
        form.append('materialFile', materials[0]);
      }

      let payload = {
        body: form,
        type: 'form-data',
      };

      await dispatchPostMaterial(payload);
      payload = {
        paramsId: course.id,
      };
      await dispatchFetchMaterials(payload);
      await dispatchGetMyCourses();

      // setTitle('');
      // setDesc('');
      // setMaterials([]);

      navigation.goBack();
    } catch (error) {
      console.log(error);
    }
  };

  const updateMaterialHandler = async () => {
    const form = new FormData();
    form.append('courseId', course.id);
    form.append('title', title);
    form.append('desc', desc);
    if (materials.length === 1) {
      form.append('materialFile', materials[0]);
    }

    let payload = {};

    // update detail
    payload = {
      body: {
        id: material.id,
        courseId: course.id,
        title: title,
        desc: desc,
      },
    };
    await dispatchUpdateMaterial(payload);

    // update file
    if (materials.length === 1) {
      payload = {
        body: form,
        type: 'form-data',
      };
      await dispatchUpdateFileMaterial(payload);
    }

    payload = {
      paramsId: course.id,
    };
    await dispatchFetchMaterials(payload);
    await dispatchGetMyCourses();

    setTitle('');
    setDesc('');
    setMaterials([]);
  };

  const EmptyMaterials = () => {
    return (
      <View style={styles.emptyMaterial}>
        <TouchableOpacity
          style={styles.addMaterialVideo}
          onPress={() => selectVideoHandler()}>
          <EvaIcon
            name={'video-outline'}
            width={24}
            height={24}
            fill={'white'}
            style={styles.btnIcon}
          />
          <Text style={styles.addMaterialText}> Video</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.addMaterialAudio}
          onPress={() => selectAudioHandler()}>
          <EvaIcon
            name={'headphones-outline'}
            width={24}
            height={24}
            fill={'white'}
            style={styles.btnIcon}
          />
          <Text style={styles.addMaterialText}> Audio</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.addMaterialPDF}
          onPress={() => selectFileHandler()}>
          <EvaIcon
            name={'archive-outline'}
            width={24}
            height={24}
            fill={'white'}
            style={styles.btnIcon}
          />
          <Text style={styles.addMaterialText}> PDF</Text>
        </TouchableOpacity>
      </View>
    );
  };

  const PdfMaterialCard = ({ item, index }) => {
    // const { thumbnail } = item;
    return (
      <View style={styles.pdfCard} key={index}>
        {/* <Image
          uri={thumbnail.uri}
          width={thumbnail.width}
          height={thumbnail.height}
        /> */}
        <Text style={styles.pdfLabel}>{item.name}</Text>
        <EvaIcon
          name="close-circle-outline"
          width={24}
          height={24}
          fill={'red'}
          onPress={() => removeFileHandler(index)}
        />
      </View>
    );
  };

  const VideoMaterialCard = ({ item, index }) => {
    return (
      <View style={styles.videoCard} key={index}>
        <View style={styles.videoDetail}>
          <Text style={styles.videoLabel}>
            {item.name ? item.name : 'Video'}
          </Text>
          <EvaIcon
            style={styles.videoCloseBtn}
            name="close-circle-outline"
            width={30}
            height={30}
            fill={'red'}
            onPress={() => removeFileHandler(index)}
          />
        </View>

        <VideoPlayer
          source={{ uri: item.uri != undefined ? item.uri : null }}
          ref={playerRef}
          toggleResizeModeOnFullscreen={false}
          tapAnywhereToPause={true}
          // controls={true}
          paused={true}
          // fullscreen={true}
          resizeMode={'contain'}
          style={styles.videoStyle}
        />
      </View>
    );
  };

  const MaterialList = () => {
    return materials.map((item, index) => {
      return (
        <View key={index} style={styles.materialContainer}>
          <View style={styles.materialCard}>
            {item.type === 'application/pdf' ? (
              <PdfMaterialCard item={item} index={index} />
            ) : (
              <VideoMaterialCard item={item} index={index} />
            )}
          </View>
        </View>
      );
    });
  };

  const CurrentVideo = () => {
    return (
      <View style={styles.currentVideo}>
        <Text style={styles.inputLabel}>Current File/Video/Audio</Text>
        <Video
          source={{ uri: `${base_url}/files/course/material/${material.path}` }}
          ref={(ref) => {
            this.player = ref;
          }} // Store reference
          controls={true}
          paused={true}
          style={styles.videoStyle}
        />
      </View>
    );
  };

  useEffect(() => {
    if (material == undefined) {
      setTitle('');
      setDesc('');
      setMaterials([]);
    } else {
      setTitle(material.title);
      setDesc(material.desc);
      // setMaterials([]);
    }

    return () => {
      setTitle('');
      setDesc('');
      setMaterials([]);
    };
  }, []);

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={styles.container}>
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <SafeAreaView style={styles.body}>
          <StatusBar
            translucent
            barStyle="dark-content"
            backgroundColor={Color.backgroundColor}
          />
          <View style={styles.header}>
            <BackButton onPress={() => navigation.goBack()} />
            <Text bold style={styles.headerLabel}>
              {course.title}
            </Text>
          </View>

          <ScrollView style={styles.form}>
            <View style={styles.formGroup}>
              <Text style={styles.inputLabel}>Material Title</Text>
              <TextInput
                style={styles.inputStyle}
                value={title}
                onChangeText={(value) => setTitle(value)}
              />
            </View>

            <View style={styles.formGroup}>
              <Text style={styles.inputLabel}>Material Decription</Text>
              <TextInput
                style={[styles.inputStyle, styles.textArea]}
                multiline={true}
                textAlignVertical="top"
                value={desc}
                onChangeText={(value) => setDesc(value)}
              />
            </View>

            <View style={styles.formGroup}>
              <Text style={styles.inputLabel}>
                Material Media{' '}
                <Text style={styles.labelMark}>( Video, Audio or PDF )</Text>
              </Text>
              <View style={styles.selectedMaterials}>
                {/* <CurrentVideo /> */}
                {/* {material && <CurrentVideo />} */}
                {materials.length > 0 ? (
                  <View>
                    <MaterialList />
                    {/* <AddMaterialBtn /> */}
                  </View>
                ) : (
                  <View>
                    <EmptyMaterials />
                  </View>
                )}
              </View>
            </View>

            <Button
              title={'Save'}
              buttonStyle={styles.saveBtn}
              textStyle={styles.saveBtnLabel}
              onPress={() =>
                material === undefined
                  ? addMaterialHandler()
                  : updateMaterialHandler()
              }
              isLoading={isLoading}
            />
          </ScrollView>
        </SafeAreaView>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  );
};

function mapStateToProps(state) {
  return {
    //isi state dari reducer
    isLoading: state.myCourseStore.isLoading,
    error: state.myCourseStore.error,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    //isi dispatch
    dispatchGetMyCourses: () => dispatch(fetchMyCourse()),
    dispatchFetchMaterials: (payload) => dispatch(getMaterials(payload)),
    dispatchPostMaterial: (payload) => dispatch(postMateri(payload)),
    dispatchUpdateMaterial: (payload) => dispatch(updateMateri(payload)),
    dispatchUpdateFileMaterial: (payload) =>
      dispatch(updateFileMateri(payload)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(MaterialAdd);
