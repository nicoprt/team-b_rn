import {
  React,
  useEffect,
  useState,
  connect,
  View,
  Image,
  TextInput,
  EvaIcon,
  Pressable,
} from 'libraries';
import { Text, Button } from 'components';
import styles from './style';
// import { scale } from 'utils';
import {
  EditProfileAction,
  GetProfileAction,
  OnChangeEmail,
  OnChangeFullname,
  OnChangePassword,
  OnChangeUsername,
} from '../../configs/redux/reducers/profile/profileActions';
// import Toast from 'react-native-toast-message';
import { Overlay } from 'react-native-elements';
// import { Icon } from 'react-native-eva-icons';
import LottieView from 'lottie-react-native';
import { StatusBar } from 'react-native';
import { Color, scale } from 'utils';

const EditPage = (props) => {
  const {
    navigation,
    dispatchGetProfile,
    dispatchEditProfile,
    dispatchOnFullname,
    dispatchOnUsername,
    dispatchOnEmail,
    dispatchOnPassword,
    fullname,
    username,
    email,
    password,
  } = props;

  // const [username, setUserName] = useState('')
  // const [fullname, setFullName] = useState('')
  // const [email, setEmail] = useState('')
  // const [password, setPassword] = useState('')
  // const [isValid, setIsValid] = useState({
  //   username: true,
  //   fullname: true,
  //   email: true,
  //   password: true
  // })

  // const [isValid, setIsValid] = useState(false);
  const [isFullName, setIsFullName] = useState(true);
  const [isUserName, setIsUserlName] = useState(true);
  const [isEmail, setIsEmail] = useState(true);
  const [isPassword, setIsPassword] = useState(true);

  const [editFullname, setFullName] = useState('');
  const [editUsername, setUserName] = useState('');
  const [editEmail, setEmail] = useState('');
  // const [editPassword, setPassword] = useState('');

  const [editStatus, seteditStatus] = useState(false);

  const [visible, setVisible] = useState(false);
  const [message, setMessage] = useState('');

  useEffect(() => {
    dispatchGetProfile();
  }, [dispatchGetProfile]);

  useEffect(() => {}, []);

  const submitEdit = () => {
    // const check = validation();
    // console.log(editFullname);
    // console.log(editUsername);
    // console.log(editEmail);

    if (!editStatus) {
      setVisible(true);
      setMessage('no changes data profile');
    } else {
      let updateFullname = editFullname === '' ? fullname : editFullname;
      let updateUsername = editUsername === '' ? username : editUsername;
      let updateEmail = editEmail === '' ? email : editEmail;
      //  console.log(updateFullname);
      //  console.log(updateUsername);
      //  console.log(updateEmail);
      const payload = {
        type: 'application/json',
        body: {
          username: updateUsername,
          fullname: updateFullname,
          email: updateEmail,
        },
      };
      dispatchEditProfile(payload);
      setVisible(true);
      setMessage('edit success');
    }
  };

  // const validation = () => {
  //   const isUserName = /^[a-z0-9]+$/i.test(username);
  //   const isFullname = /^[a-z\d\-_\s]+$/i.test(fullname);
  //   const isEmail = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(
  //     email,
  //   );
  //   const isPassword = /^\S*$/.test(password);
  //   setIsValid({
  //     ...isValid,
  //     username: isUserName,
  //     fullname: isFullname,
  //     email: isEmail,
  //     password: isPassword,
  //   });
  //   console.log(isFullname);
  //   if (isUserName && isFullname && isPassword && isEmail) {
  //     console.log({
  //       body: {
  //         username,
  //         fullname,
  //         password,
  //         email,
  //       },
  //     });
  //     dispatchEditProfile({
  //       body: {
  //         username,
  //         fullname,
  //         password,
  //         email,
  //       },
  //     });
  //   }
  // };

  return (
    <View style={styles.edit1}>
      <StatusBar
        backgroundColor={Color.backgroundColor}
        barStyle={'dark-content'}
      />
      <View>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginTop: scale(10),
            marginBottom: scale(20),
            marginLeft: scale(10),
          }}>
          <Pressable
            onPress={() => {
              navigation.pop();
            }}>
            <View
              style={{
                backgroundColor: Color.primaryColor,
                borderRadius: scale(50),
                padding: scale(4),
              }}>
              <EvaIcon
                name={'arrow-ios-back-outline'}
                width={scale(20)}
                height={scale(20)}
                fill={'white'}
              />
            </View>
          </Pressable>

          <Text
            bold
            style={{
              fontSize: scale(25),
              color: Color.primaryColor,
              marginLeft: scale(10),
              textTransform: 'capitalize',
            }}>
            Edit Profile
          </Text>
        </View>

        <View
          style={[
            styles.edit2,
            {
              marginTop: scale(100),
            },
          ]}>
          <Image
            style={styles.edit3}
            source={require('../../assets/images/human.png')}
          />
          <TextInput
            placeholder={'Fullname'}
            autoCapitalize="none"
            onChangeText={(nama) => {
              const checkFullname = /^[a-z\d\-_\s]+$/i.test(nama);
              setIsFullName(checkFullname);
              setFullName(nama);
              seteditStatus(true);

              console.log(checkFullname);
              const payload = {
                fullname: nama,
              };
              dispatchOnFullname(payload);
            }}
            color="#1E59B5"
            value={fullname}
            style={{ width: '100%' }}
          />
          {/* {(!isValid)?<Text style={{color:'#2D8989', marginLeft:scale(50),
          marginRight:scale(50),}}>accept only alpha space</Text>:<View></View>} */}
        </View>
        <View>
          {!isFullName ? (
            <Text style={styles.edit4}>accept only alpha space</Text>
          ) : (
            <View />
          )}
        </View>
      </View>

      <View>
        <View
          style={[
            styles.edit2,
            {
              borderColor: !isUserName ? 'red' : Color.primaryColor,
              backgroundColor: '#f1f2f6',
            },
          ]}>
          <Image
            style={styles.edit6}
            source={require('../../assets/images/nama.png')}
          />
          <TextInput
            // onValidation={(check) =>
            //   setIsValid({ ...isValid, username: check })
            // }
            editable={false}
            autoCapitalize="none"
            placeholder={'Username'}
            onChangeText={(nama) => {
              const checkUserName = /^[a-z0-9]+$/i.test(nama);
              setIsUserlName(checkUserName);
              setUserName(nama);
              seteditStatus(true);

              console.log(checkUserName);
              const payload = {
                username: nama,
              };
              dispatchOnUsername(payload);
            }}
            color="#1E59B5"
            value={username}
            style={{ width: '100%' }}
          />
          {/* {(!isValid.username)?<Text style={{color:'#2D8989',marginLeft:scale(50),
           marginRight:scale(50),}}>accept only alpha numeric</Text>:<View></View>} */}
        </View>
        <View>
          {!isUserName ? (
            <Text style={styles.edit7}>accept only alpha numeric</Text>
          ) : (
            <View />
          )}
        </View>
      </View>

      <View>
        <View
          style={[
            styles.edit2,
            {
              borderColor: !isEmail ? 'red' : Color.primaryColor,
              backgroundColor: '#f1f2f6',
            },
          ]}>
          <Image
            style={styles.edit9}
            source={require('../../assets/images/email.png')}
          />
          <TextInput
            editable={false}
            placeholder={'Email'}
            autoCapitalize="none"
            onChangeText={(nama) => {
              const checkEmail = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(
                email,
              );
              setIsEmail(checkEmail);
              setEmail(nama);
              seteditStatus(true);

              console.log(checkEmail);
              const payload = {
                email: nama,
              };
              dispatchOnEmail(payload);
            }}
            color="#1E59B5"
            value={email}
            style={{ width: '100%' }}
          />
          {/* {(!isValid.email)?<Text style={{color:'#2D8989', marginLeft:scale(50),
          marginRight:scale(50),}}>accept only email type</Text>:<View></View>} */}
        </View>
        <View>
          {!isEmail ? (
            <Text style={styles.edit10}>accept only email type</Text>
          ) : (
            <View />
          )}
        </View>
      </View>

      {/* <View>
        <View style={[styles.edit2, { borderColor: 'lightgray' }]}>
          <Image
            style={styles.edit12}
            source={require('../../assets/images/password.png')}
          />
          <TextInput
            secureTextEntry={true}
            placeholder={'Password'}
            onChangeText={(nama) => {
              const checkPassword = /^\S*$/.test(password);
              setIsPassword(checkPassword);

              console.log(checkPassword);
              const payload = {
                password: nama,
              };
              dispatchOnPassword(payload);
            }}
            color="#1E59B5"
            value={'***********'}
            style={{ width: '100%' }}
            editable={false}
          />
        </View>
        <View>
          {!isPassword ? (
            <Text style={styles.edit13}>accept only non whitespace</Text>
          ) : (
            <View />
          )}
        </View>
      </View> */}

      <View style={styles.edit14}>
        <Button
          buttonStyle={styles.edit18}
          textStyle={styles.edit19}
          title={'SAVE'}
          onPress={() => {
            submitEdit();
          }}
        />
      </View>
      <Overlay
        isVisible={visible}
        onBackdropPress={() => setVisible(false)}
        overlayStyle={styles.edit15}>
        <View style={styles.edit16}>
          {/* <Icon name="email-outline"
                width={scale(30)}
                height={scale(30)}
                fill={'white'}></Icon> */}
          <LottieView
            source={require('../../assets/animasi/popup.json')}
            autoPlay
            loop
          />
          <Text style={styles.edit17}>{message}</Text>
        </View>
      </Overlay>
    </View>
  );
};

function mapStateToProps(state) {
  return {
    fullname: state.profileStore.fullname,
    username: state.profileStore.username,
    email: state.profileStore.email,
    password: state.profileStore.password,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatchGetProfile: () => dispatch(GetProfileAction()),
    dispatchEditProfile: (payload) => dispatch(EditProfileAction(payload)),
    dispatchOnFullname: (payload) => dispatch(OnChangeFullname(payload)),
    dispatchOnUsername: (payload) => dispatch(OnChangeUsername(payload)),
    dispatchOnEmail: (payload) => dispatch(OnChangeEmail(payload)),
    dispatchOnPassword: (payload) => dispatch(OnChangePassword(payload)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EditPage);
