import {
  React,
  useEffect,
  useState,
  connect,
  View,
  Image,
  StatusBar,
  EvaIcon,
  Pressable,
  TouchableOpacity,
} from 'libraries';
import { Text, Button } from 'components';
import styles from './style';
// import { scale } from 'utils';
import LottieView from 'lottie-react-native';
import ActionButton from 'react-native-action-button';
import baseUrl from '../../configs/api/url';
import * as RootNavigation from '../../configs/routes/RootNavigation';
import * as ImagePicker from 'react-native-image-picker';
import { UpdateImageAction } from 'configs/redux/reducers/profile/profileActions';
import Toast from 'react-native-simple-toast';
import { Color, scale } from 'utils';

const AvatarPage = (props) => {
  const { dispatchUpdateImage, profilePicture, isLoading } = props;

  // const imageBlank = Image.resolveAssetSource(
  //   '../../assets/images/blankpicture.png',
  // );

  useEffect(() => {
    // if (profilePicture == null) {
    //   seturiImage(null);
    // } else {
    //   seturiImage(baseUrl.profil.getprofilepicture + profilePicture);
    // }
    seturiImage(baseUrl.profil.getprofilepicture + profilePicture);
    // console.log(imageBlank);
  }, []);

  // useEffect(() => {}, []);

  const [isValid, setIsValid] = useState(false);
  const [uriImage, seturiImage] = useState(null);
  // const [message, setMessage] = useState('');

  const uploadImage = () => {
    ImagePicker.launchImageLibrary(
      {
        mediaType: 'photo',
        includeBase64: false,
        quality: 1,
      },
      (response) => {
        if (response.didCancel) {
          Toast.show('Cancel', Toast.LONG);
        } else {
          console.log('test');
          console.log(response);
          //setResponse(response);
          // const form = new FormData();
          // form.append('profilePicture', {
          //   uri: response.uri,
          //   type: 'image/jpg',
          //   name: 'avatar.jpg',
          // });
          // const payload = {
          //   body: form,
          //   type: 'form-data',
          // };
          seturiImage(response.uri);
          // console.log(form.profilePicture.type)
          setIsValid(true);
        }
      },
    );
  };

  const doSave = () => {
    if (isValid) {
      const form = new FormData();
      form.append('profilePicture', {
        uri: uriImage,
        type: 'image/jpg',
        name: 'avatar.jpg',
      });
      const payload = {
        body: form,
        type: 'form-data',
      };
      dispatchUpdateImage(payload);
    } else {
      Toast.show('Profile picture not update', Toast.LONG);
    }
  };

  return (
    <View style={styles.container}>
      <StatusBar
        backgroundColor={Color.primaryColor}
        barStyle={'light-content'}
      />
      <View
        style={{
          height: scale(30),
          backgroundColor: Color.primaryColor,
        }}
      />
      <View style={styles.avatar1}>
        <LottieView
          source={require('../../assets/animasi/camera.json')}
          autoPlay
          loop
        />
      </View>

      <View style={styles.avatar3}>
        <View style={styles.avatar4}>
          <Text style={styles.avatar5}>Profile Picture</Text>
          <Text style={styles.avatar10}> Upload your's photos</Text>
        </View>

        <View style={styles.avatar7}>
          <View>
            <Image
              // source={require('../../assets/images/foto.jpg')}
              source={{
                uri:
                  uriImage == null
                    ? 'https://ualr.edu/studentaffairs/files/2020/01/blank-picture-holder.png'
                    : uriImage,
              }}
              // source={require('../../assets/images/blankpicture.png')}
              // source={
              //   uriImage == null
              //     ? require('../../assets/images/blankpicture.png')
              //     : { uri: uriImage }
              // }
              style={styles.avatar6}
            />

            {/* <Image source={{uri:uriImage}}
            style={styles.avatar6}
            ></Image> */}

            <View
              style={{
                position: 'absolute',
                bottom: 0,
                right: 0,
                backgroundColor: Color.primaryColor,
                // padding: scale(10),
                borderRadius: scale(50),
                height: scale(60),
                width: scale(60),
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <TouchableOpacity onPress={uploadImage}>
                <EvaIcon
                  name={'camera-outline'}
                  height={scale(35)}
                  width={scale(35)}
                  fill={'white'}
                />
              </TouchableOpacity>
            </View>
          </View>
          {/* <ActionButton
            buttonColor="#2D8989"
            offsetY={280}
            offsetX={100}
            size={60}
            onPress={uploadImage}
          /> */}

          <Button
            buttonStyle={styles.avatar8}
            textStyle={styles.avatar9}
            title={'SAVE'}
            onPress={() => doSave()}
            isLoading={isLoading}
          />
          {/* <Text style={{ textAlign: 'center', color: 'red' }}>{message}</Text> */}
          {/* <Toast ref={(ref) => Toast.setRef(ref)} /> */}
        </View>
      </View>
    </View>
  );
};

function mapStateToProps(state) {
  return {
    profilePicture: state.profileStore.profilePicture,
    isLoading: state.profileStore.isLoading,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatchUpdateImage: (payload) => dispatch(UpdateImageAction(payload)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AvatarPage);
