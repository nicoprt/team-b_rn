import { StyleSheet } from 'react-native';
import { Color, METRICS, scale } from 'utils';
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Color.backgroundColor,
  },
  style1: { alignItems: 'center', marginTop: scale(20) },
  style2: { color: '#2D8989', fontSize: scale(40), fontWeight: 'bold' },
  style3: {
    flex: 1,
    justifyContent: 'center',
    marginHorizontal: scale(30),
  },
  style4: { marginBottom: scale(200) },
  style5: { borderColor: '#2D8989' },
  style6: { borderColor: '#2D8989' },
  style7: { color: '#2D8989' },
  style8: { color: '#2D8989' },
  style9: { borderColor: '#2D8989' },
  style10: { color: '#2D8989' },
  style11: { color: '#2D8989' },
  style12: { alignItems: 'center' },
  style13: {
    marginTop: scale(15),
    color: '#2D8989',
    fontSize: scale(15),
  },
  style14: {
    color: '#2D8989',
    fontWeight: 'bold',
    fontSize: scale(15),
  },

  style15: {
    marginTop: scale(30),
    backgroundColor: '#2D8989',
    paddingVertical: scale(15),
    borderRadius: scale(50),
    width: METRICS.window.width * 0.8,
  },

  style16: {
    fontSize: scale(15),
    fontWeight: 'bold',
    color: 'white',
  },

  style17: {
    flexDirection: 'row',
    marginHorizontal: scale(10),
    // borderWidth: 2,
    borderRadius: scale(50),
    // borderColor: '#2D8989',
    marginTop: scale(60),
    backgroundColor: 'white',
    color: '#2D8989',
    alignItems: 'center',
    paddingVertical: scale(5),
  },

  style18: {
    padding: scale(10),
    margin: scale(10),
    height: scale(20),
    width: scale(20),
    resizeMode: 'stretch',
  },

  style19: {
    flexDirection: 'row',
    marginHorizontal: scale(10),
    // borderWidth: 2,
    borderRadius: scale(50),
    // borderColor: '#2D8989',
    marginTop: scale(15),
    backgroundColor: 'white',
    color: '#2D8989',
    alignItems: 'center',
    paddingVertical: scale(5),
  },

  style20: {
    padding: scale(10),
    margin: scale(10),
    height: scale(20),
    width: scale(20),
    resizeMode: 'stretch',
  },
});

export default styles;
