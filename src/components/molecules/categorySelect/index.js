import { Text } from 'components/atoms';
import { getCourseCategory } from 'configs/redux/reducers/course/courseActions';
import { setSelectedCategory } from 'configs/redux/reducers/myCourse/myCourseActions';
import {
  connect,
  EvaIcon,
  FlatList,
  React,
  TextInput,
  View,
  useEffect,
  useState,
  TouchableOpacity,
} from 'libraries';
import styles from './style';

const CategorySelect = (props) => {
  const {
    sheetRef,
    courseCategories,
    dispatchGetCourseCatgories,
    dispatchSetSelectedCategories,
  } = props;
  const [search, setSearch] = useState('');
  const [categories, setCategories] = useState([]);

  useEffect(() => {
    dispatchGetCourseCatgories();
  }, []);

  useEffect(() => {
    setCategories(courseCategories.data);
  }, [courseCategories]);

  const CategoryTapHandler = (item) => {
    dispatchSetSelectedCategories(item);
    sheetRef.current.snapTo(2);
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Category</Text>
      {/* <View style={styles.searchContainer}>
        <EvaIcon name="search" width={20} height={20} fill={'black'} />
        <TextInput
          style={styles.searchField}
          placeholder={'Find Category'}
          returnKeyType="search"
          value={search}
          onChangeText={(value) => setSearch(value)}
          onSubmitEditing={() => {}}
        />
      </View> */}
      <FlatList
        data={categories}
        keyExtractor={(item) => item.id.toString()}
        ListEmptyComponent={() => (
          <View style={styles.emptyCategories}>
            <Text>No Categories</Text>
          </View>
        )}
        renderItem={({ item, index }) => {
          return (
            <View style={styles.categories} pointerEvents="auto">
              <TouchableOpacity
                style={styles.tcOpacity}
                onPress={() => CategoryTapHandler(item)}>
                <Text style={styles.categoryLabel}>{item.name}</Text>
              </TouchableOpacity>
            </View>
          );
        }}
      />
    </View>
  );
};

function mapStateToProps(state) {
  return {
    //isi state dari reducer
    courseCategories: state.courseStore.course_category,
    isLoading: state.searchStore.isLoading,
    error: state.searchStore.error,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    //isi dispatch
    dispatchGetCourseCatgories: () => dispatch(getCourseCategory()),
    dispatchSetSelectedCategories: (payload) =>
      dispatch(setSelectedCategory(payload)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CategorySelect);
