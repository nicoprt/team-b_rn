import { base_url } from 'configs/api/url';
import { EvaIcon, Image, React, Text, TouchableOpacity, View } from 'libraries';
import { Color } from 'utils';
import styles from './style';

const MyCourseCard = (props) => {
  const { item, onPress = {} } = props;

  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.touchableArea} onPress={onPress}>
        <View style={styles.imageContainer}>
          <Image
            style={styles.cardImage}
            source={{
              uri:
                item.picture !== null
                  ? `${base_url}/files/course/image/${item.picture}`
                  : 'https://akm-img-a-in.tosshub.com/indiatoday/images/story/201811/online-3412473_1920_1.jpeg',
            }}
          />
        </View>
        <View style={styles.detailContainer}>
          <View>
            <Text style={styles.title} numberOfLines={1}>
              {item.title}
            </Text>
            <View style={[styles.detailRow, styles.statusContainer]}>
              <EvaIcon
                name={
                  item.isPublished
                    ? 'checkmark-circle-outline'
                    : 'slash-outline'
                }
                width={16}
                height={16}
                fill={item.isPublished ? 'green' : 'red'}
                style={styles.detailIcon}
              />
              <Text style={styles.publishLabel}>
                {item.isPublished ? 'Published' : 'Not Published'}
              </Text>
            </View>
            <View style={[styles.detailRow, styles.subscriberContainer]}>
              <EvaIcon
                name="people-outline"
                width={16}
                height={16}
                fill={Color.primaryColor}
                style={styles.detailIcon}
                ƒ
              />
              <Text style={styles.subscriber}>{item.subscriberCount}</Text>
            </View>
          </View>

          <Text style={styles.information}>Tap to edit</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default MyCourseCard;
