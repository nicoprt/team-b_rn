import { base_url } from 'configs/api/url';
import { EvaIcon, Image, React, Text, TouchableOpacity, View } from 'libraries';
import styles from './style';
import { Icon } from 'react-native-eva-icons';
import { scale } from 'utils';

const MySubscriptionCard = (props) => {
  const { item, onPress = {} } = props;

  //   {
  //     "id": 23,
  //     "userId": "e47c1727-6b6a-5c05-b6da-1b62cd8673ca",
  //     "courseId": 14,
  //     "isCompleted": true,
  //     "mark": 100,
  //     "rating": 6,
  //     "comment": "good joooobs",
  //     "updatedAt": "2021-02-10T04:21:23.312Z",
  //     "courseDetail": {
  //         "title": "Dasar-dasar PHP",
  //         "desc": "PHP versi 7 dengan engine terbaru",
  //         "picture": "1613401700023-course-img.jpg"
  //     }
  // }

  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.touchableArea} onPress={onPress}>
        <View style={styles.imageContainer}>
          <Image
            style={styles.cardImage}
            source={{
              uri:
                item.courseDetail?.picture !== null
                  ? `${base_url}/files/course/image/${item.courseDetail?.picture}`
                  : 'https://akm-img-a-in.tosshub.com/indiatoday/images/story/201811/online-3412473_1920_1.jpeg',
            }}
          />
        </View>
        <View style={styles.detailContainer}>
          <View>
            <Text style={styles.title} numberOfLines={1}>
              {item.courseDetail?.title}
            </Text>
            <Text style={styles.desc} numberOfLines={2}>
              {item.courseDetail?.desc}
            </Text>
            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 4 }}>
              <Icon
                name={'star'}
                height={scale(14)}
                width={scale(14)}
                fill={'#FF9D2A'}
              />
              <Text bold style={{ color: 'black', fontSize: scale(10) }}>
                {item.rating ? Math.round(
                  (parseFloat(item.rating) + Number.EPSILON) * 100,
                ) / 100 : 'No rating yet'}
              </Text>
            </View>

            {/* <View style={styles.statusContainer}>
              <EvaIcon
                name={
                  item.isPublished
                    ? 'checkmark-circle-outline'
                    : 'slash-outline'
                }
                width={16}
                height={16}
                fill={item.isPublished ? 'green' : 'red'}
              />
              <Text style={styles.publishLabel}>
                {item.isPublished ? 'Published' : 'Not Published'}
              </Text>
            </View> */}
          </View>

          <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginTop: 10 }}>
            <View style={styles.statusContainer}>
              <EvaIcon
                name={
                  item.isCompleted
                    ? 'checkmark-circle-outline'
                    : 'slash-outline'
                }
                width={16}
                height={16}
                fill={item.isCompleted ? 'green' : 'red'}
              />
              <Text style={styles.publishLabel}>
                {item.isCompleted ? 'Completed' : 'Not Completed'}
              </Text>
            </View>
            <Text style={styles.desc} numberOfLines={1}>
              {`Your Score : ${item.mark ? item.mark : '-'}`}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default MySubscriptionCard;
