import { StyleSheet } from 'react-native';
import { Color, scale, METRICS } from 'utils';
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: scale(10),
    justifyContent: 'space-between',
  },
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: scale(10),
  },
  avatarWrapper: {
    elevation: 5,
    height: scale(50),
    width: scale(50),
    borderRadius: scale(25),
    resizeMode: 'cover',
  },
  avatar: {
    height: scale(50),
    width: scale(50),
    borderRadius: scale(25),
  },
  textWrapper: {
    marginLeft: scale(10),
    width: METRICS.window.width * 0.6,
  },
  title: { fontSize: scale(17), color: '#3E5481' },
  message: { color: '#9FA5C0', fontSize: scale(13) },
  time: { color: '#9FA5C0', fontSize: scale(10) },
  imageWrapper: {
    elevation: 5,
    height: scale(60),
    width: scale(60),
    borderRadius: scale(10),
    resizeMode: 'cover',
  },
  image: {
    height: scale(60),
    width: scale(60),
    borderRadius: scale(10),
    resizeMode: 'cover',
  },
});

export default styles;
