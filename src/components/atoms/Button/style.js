import { scale, Color } from 'utils';

const styles = {
  container: {
    flex: 1,
  },
  button: {
    backgroundColor: Color.primaryColor,
    paddingHorizontal: scale(25),
    paddingVertical: scale(15),
    justifyContent: 'center',
    alignItems: 'center',
    margin: scale(5),
    borderRadius: scale(50),
    // marginTop: scale(50),
    // minWidth: 350,
    // minHeight: scale(50),
  },
  text: {
    fontSize: scale(15),
    fontWeight: 'bold',
    color: 'white',
  },
};

export default styles;
