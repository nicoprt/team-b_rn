import { StyleSheet } from 'libraries';
import { Color, scale } from 'utils';

const styles = StyleSheet.create({
  btnBody: {
    backgroundColor: Color.primaryColor,
    position: 'absolute',
    bottom: scale(10),
    right: scale(10),
    width: scale(50),
    height: scale(50),
    borderRadius: scale(100),
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 999,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
});

export default styles;
