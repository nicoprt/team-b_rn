import { React, TextInput as RNTextInput } from 'libraries';
import { FONTS } from 'utils/styles';
import PropTypes from 'prop-types';

const TextInput = ({ children, bold, medium, style, ...props }) => {
  return (
    <RNTextInput
      {...props}
      style={[
        style,
        {
          fontFamily: bold
            ? FONTS.brandonGrotesqueBold
            : medium
            ? FONTS.brandonGrotesqueMedium
            : FONTS.brandonGrotesque,
        },
      ]}>
      {children}
    </RNTextInput>
  );
};

TextInput.propTypes = {
  bold: PropTypes.bool,
  medium: PropTypes.bool,
};

TextInput.defaultProps = {
  bold: false,
  medium: false,
};

export default TextInput;
