import { EvaIcon, React, TouchableOpacity } from 'libraries';
import { Color, scale } from 'utils';
import styles from './style';

const BackButton = (props) => {
  const { onPress, disabled = false } = props;

  return (
    <TouchableOpacity
      disabled={disabled}
      style={styles.backButton}
      onPress={onPress}>
      <EvaIcon
        name={'arrow-ios-back-outline'}
        width={scale(20)}
        height={scale(20)}
        fill={'white'}
      />
    </TouchableOpacity>
  );
};

export default BackButton;
