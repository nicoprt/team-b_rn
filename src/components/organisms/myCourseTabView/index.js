import { Button, Text } from 'components/atoms';
import {
  connect,
  // EvaIcon,
  FlatList,
  React,
  // useState,
  useEffect,
  useIsFocused,
  View,
} from 'libraries';
import styles from './style';
import EmptySubscription from 'assets/images/emptyMyCourse.svg';
import { scale, verticalScale } from 'utils';
import { fetchMyCourse } from 'configs/redux/reducers/myCourse/myCourseActions';
import { MyCourseCard } from 'components/molecules';
import FloatingButton from 'components/atoms/FloatingButton';

const MyCourseTabView = (props) => {
  const { navigation, myCourses, dispatchGetMyCourses } = props;
  const isFocused = useIsFocused();

  useEffect(() => {
    dispatchGetMyCourses();
  }, []);

  useEffect(() => {
    if (isFocused) {
      dispatchGetMyCourses();
    }
  }, [isFocused]);

  return (
    <View style={styles.container}>
      {myCourses.length > 0 && (
        <FloatingButton
          onPress={() => navigation.navigate('CourseCreatePage')}
        />
      )}
      <FlatList
        showsVerticalScrollIndicator={false}
        data={myCourses}
        keyExtractor={(item) => item.id.toString()}
        contentContainerStyle={styles.flatListContainer}
        contentInset={{ bottom: scale(100) }}
        ListEmptyComponent={() => {
          return (
            <View style={styles.empty}>
              <EmptySubscription width={scale(250)} height={scale(250)} />
              <Text style={styles.emptyText1}>
                Want to share some knowledge?
              </Text>
              <Text style={styles.emptyText2}>
                Let's create your first course
              </Text>
              <View style={styles.emptyBtnAddCotainer}>
                <Button
                  title="Create a Course"
                  textStyle={styles.emptyBtnTextAddCourse}
                  buttonStyle={styles.emptyBtnAddCourse}
                  onPress={() => {
                    navigation.navigate('CourseCreatePage');
                  }}
                />
              </View>
            </View>
          );
        }}
        renderItem={({ item }) => {
          return (
            <MyCourseCard
              item={item}
              onPress={() => {
                navigation.navigate('CourseEditPage', { data: item });
              }}
            />
          );
        }}
      />
    </View>
  );
};

function mapStateToProps(state) {
  return {
    //isi state dari reducer
    myCourses: state.myCourseStore.myCourses,
    isLoading: state.myCourseStore.isLoading,
    error: state.myCourseStore.error,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    //isi dispatch
    dispatchGetMyCourses: () => dispatch(fetchMyCourse()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(MyCourseTabView);
