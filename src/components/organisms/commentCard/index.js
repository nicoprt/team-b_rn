import ReadMore from '@fawazahmed/react-native-read-more';
import { Text } from 'components/atoms';
import { base_url } from 'configs/api/url';
import { Image, moment, React, View } from 'libraries';
import PropTypes from 'prop-types';
import { Icon } from 'react-native-eva-icons';
import { FONTS, scale } from 'utils';
import styles from './style';

const CommentCard = (props) => {
  const { avatar, fullname, rating, updateTime, comment } = props;
  console.log('avatar', avatar);
  return (
    <View style={{ marginBottom: scale(5) }}>
      <View
        style={{
          flexDirection: 'row',
          backgroundColor: '#F4F4F4',
          borderRadius: scale(10),
          padding: scale(10),
        }}>
        <Image
          source={{
            uri:
              avatar !== null
                ? `${base_url}/files/user/thumb/${avatar}`
                : 'https://ppid.sulselprov.go.id/assets/front/img/kadis/avatar.png',
          }}
          style={{
            width: scale(39),
            height: scale(39),
            borderRadius: scale(30),
          }}
        />
        <View style={{ marginLeft: scale(5), flex: 1 }}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}>
            <View>
              <Text bold>{fullname}</Text>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Icon
                  name={'star'}
                  height={scale(15)}
                  width={scale(15)}
                  fill={'#FF9D2A'}
                />
                <Text bold style={{ color: 'black' }}>
                  {Math.round((parseFloat(rating) + Number.EPSILON) * 100) /
                    100}
                </Text>
              </View>
            </View>
            <Text style={{ alignSelf: 'flex-start', color: '#8F9BB3' }}>
              {moment(updateTime).fromNow()}
            </Text>
          </View>
          <View
            style={
              {
                // paddingRight: scale(45),
              }
            }>
            <ReadMore
              numberOfLines={5}
              style={{
                fontSize: scale(13),
                fontFamily: FONTS.brandonGrotesque,
              }}
              backgroundColor={'#F4F4F4'}
              seeMoreStyle={{
                color: 'red',
                fontSize: scale(13),
                fontFamily: FONTS.brandonGrotesque,
              }}>
              {comment}
            </ReadMore>
          </View>
        </View>
      </View>
    </View>
  );
};

CommentCard.propTypes = {
  avatar: PropTypes.string,
  fullname: PropTypes.string,
  rating: PropTypes.number,
  updateTime: PropTypes.string,
  comment: PropTypes.string,
};

CommentCard.defaultProps = {
  avatar: null,
  fullname: '',
  rating: 0,
  updateTime: '',
  comment: '',
};

export default React.memo(CommentCard);
