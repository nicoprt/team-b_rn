// RootNavigation.js

import { CommonActions, StackActions } from '@react-navigation/native';
import * as React from 'react';

export const isReadyRef = React.createRef();

export const navigationRef = React.createRef();

export const stackNavigationRef = React.createRef();

export function navigate(name, params) {
  if (isReadyRef.current && navigationRef.current) {
    // Perform navigation if the app has mounted
    navigationRef.current.navigate(name, params);
  } else {
    // You can decide what to do if the app hasn't mounted
    // You can ignore this, or add these actions to a queue you can call later
  }
}

export function reset(name) {
  if (isReadyRef.current && navigationRef.current) {
    // Perform navigation if the app has mounted
    navigationRef.current.reset({
      index: 0,
      routes: [{ name }],
    });
  } else {
    // You can decide what to do if the app hasn't mounted
    // You can ignore this, or add these actions to a queue you can call later
  }
}

export function replaceScreen(name, params = undefined) {
  if (isReadyRef.current && navigationRef.current) {
    // Perform navigation if the app has mounted
    const currentRoute = navigationRef.current.getCurrentRoute();
    const { routes } = navigationRef.current.getRootState();

    const newParams = params !== undefined ? params : currentRoute.params;

    navigationRef.current.reset({
      index: routes.length - 1,
      routes: [{ name: name, params: newParams }],
    });
  } else {
    // You can decide what to do if the app hasn't mounted
    // You can ignore this, or add these actions to a queue you can call later
  }
}

export function pop(index) {
  if (isReadyRef.current && navigationRef.current) {
    // Perform navigation if the app has mounted
    const currentRoute = navigationRef.current.getCurrentRoute();
    navigationRef.current.dispatch(StackActions.pop(index));
  } else {
    // You can decide what to do if the app hasn't mounted
    // You can ignore this, or add these actions to a queue you can call later
  }
}

// add other navigation functions that you need and export them
