export const dummyApiPopularSearch = () => {
  return new Promise((resolve, reject) => {
    const dummy = [
      { topicId: 1, topicName: 'Web' },
      { topicId: 2, topicName: 'Angular' },
      { topicId: 3, topicName: 'Excel' },
      { topicId: 4, topicName: 'Finance' },
      { topicId: 5, topicName: 'Network Core' },
      { topicId: 6, topicName: 'IT Securty' },
      { topicId: 7, topicName: 'Marketing' },
      { topicId: 8, topicName: 'Digital Marketing' },
      { topicId: 9, topicName: 'SEO' },
      { topicId: 10, topicName: 'Fishing' },
      { topicId: 11, topicName: 'Baiting' },
    ];

    setTimeout(() => {
      resolve(dummy);
    }, 500);
  });
};

export const dummyApiPopularTrainer = () => {
  return new Promise((resolve, reject) => {
    const dummy = [
      {
        trainerId: 1,
        trainerName: 'Agus',
        title: 'Web Programmer From Back to The Front',
        image: 'https://picsum.photos/200',
      },
      {
        trainerId: 2,
        trainerName: 'Mark Zuckenberk Saja',
        title: 'Social Specialist',
        image: 'https://picsum.photos/200',
      },
      {
        trainerId: 3,
        trainerName: 'Mas Elon',
        title: 'Businessman',
        image: 'https://picsum.photos/200',
      },
      {
        trainerId: 4,
        trainerName: 'Hattori',
        title: 'Search Engine Specialist',
        image: 'https://picsum.photos/200',
      },
      {
        trainerId: 5,
        trainerName: 'Turbo',
        title: 'MS Office Practicioner',
        image: 'https://picsum.photos/200',
      },
    ];

    setTimeout(() => {
      resolve(dummy);
    }, 500);
  });
};

export const dummyApiCategories = () => {
  return new Promise((resolve, reject) => {
    const dummy = [
      {
        categoryId: 1,
        categoryName: 'IT',
        image: 'http://placeimg.com/200/125/tech',
      },
      {
        categoryId: 2,
        categoryName: 'Digital Marketing',
        image: 'http://placeimg.com/200/125/tech',
      },
      {
        categoryId: 3,
        categoryName: 'Digitalization',
        image: 'http://placeimg.com/200/125/tech',
      },
      {
        categoryId: 4,
        categoryName: 'Finance',
        image: 'http://placeimg.com/200/125/tech',
      },
      {
        categoryId: 5,
        categoryName: 'Business',
        image: 'http://placeimg.com/200/125/tech',
      },
      {
        categoryId: 6,
        categoryName: 'Personal Development',
        image: 'http://placeimg.com/200/125/tech',
      },
    ];

    setTimeout(() => {
      resolve(dummy);
    }, 500);
  });
};

export const dummyApiGetCourse = () => {
  return new Promise((resolve, reject) => {
    const dummy = [
      {
        courseId: 1,
        courseName: 'Ngoding Cantik',
        image: 'http://placeimg.com/320/240/tech',
      },
      {
        courseId: 2,
        courseName: 'Belajar Excel Bersama',
        image: 'http://placeimg.com/320/240/tech',
      },
      {
        courseId: 3,
        courseName: 'Diajar Sunda Ayeuna',
        image: 'http://placeimg.com/320/240/tech',
      },
      {
        courseId: 4,
        courseName: 'Bahasa Isyarat Sarasvati',
        image: 'http://placeimg.com/320/240/tech',
      },
      {
        courseId: 5,
        courseName: 'Gitar untuk Bapak2',
        image: 'http://placeimg.com/320/240/tech',
      },
    ];

    setTimeout(() => {
      resolve(dummy);
    }, 500);
  });
};

export const dummyApiGetProfile = () => {
  return new Promise((resolve, reject) => {
    const dummy = [
      {
        trainerId: 1,
        trainerName: 'Ody',
        title: 'Web Programmer',
        image: 'http://placeimg.com/100/100/people',
      },
      {
        trainerId: 2,
        trainerName: 'Tsubasa',
        title: 'Business Consultant',
        image: 'http://placeimg.com/100/100/people',
      },
      {
        trainerId: 3,
        trainerName: 'Ray Mysterio',
        title: 'Michelin Star Chef',
        image: 'http://placeimg.com/100/100/people',
      },
      {
        trainerId: 4,
        trainerName: 'Mamen',
        title: 'WWU restler',
        image: 'http://placeimg.com/100/100/people',
      },
      {
        trainerId: 5,
        trainerName: 'Mark Zuckenberg',
        title: 'World Magician',
        image: 'http://placeimg.com/100/100/people',
      },
    ];

    setTimeout(() => {
      resolve(dummy);
    }, 500);
  });
};
