import API from 'configs/api';
import { expiredAction } from '../auth/authActions';

export function getCourseCategory() {
  return async (dispatch) => {
    try {
      dispatch({
        type: 'GETTING_CATEGORY',
      });
      const res = await API.getCategory();

      if (res) {
        dispatch({
          type: 'UPDATE_COURSE_CATEGORY',
          payload: res.data,
        });
      } else {
        dispatch({
          type: 'UPDATE_COURSE_CATEGORY',
          payload: [],
        });
      }
    } catch (e) {
      if (e.status === 401) {
        dispatch(expiredAction());
      }
      dispatch({
        type: 'UPDATE_COURSE_CATEGORY_FAILED',
        payload: e,
      });
    }
  };
}

export function getPopularCourse(payload) {
  return async (dispatch) => {
    try {
      dispatch({
        type: 'GETTING_POPULAR_COURSE',
      });
      const res = await API.getPopular(payload);

      if (res) {
        dispatch({
          type: 'UPDATE_POPULAR_COURSE',
          payload: res.data,
        });
      } else {
        dispatch({
          type: 'UPDATE_POPULAR_COURSE',
          payload: [],
        });
      }
    } catch (e) {
      if (e.status === 401) {
        dispatch(expiredAction());
      }
      dispatch({
        type: 'UPDATE_POPULAR_COURSE_FAILED',
        payload: e,
      });
    }
  };
}

export function getNewCourse(payload) {
  return async (dispatch) => {
    try {
      dispatch({
        type: 'GETTING_NEW_COURSE',
      });
      const res = await API.getNewCourse(payload);

      if (res) {
        dispatch({
          type: 'UPDATE_NEW_COURSE',
          payload: res.data,
        });
      } else {
        dispatch({
          type: 'UPDATE_NEW_COURSE',
          payload: [],
        });
      }
    } catch (e) {
      if (e.status === 401) {
        dispatch(expiredAction());
      }
      dispatch({
        type: 'UPDATE_NEW_COURSE_FAILED',
        payload: e,
      });
    }
  };
}

export function getRecentCourse() {
  return async (dispatch) => {
    try {
      dispatch({
        type: 'GETTING_RECENT_COURSE',
      });
      const res = await API.getRecent();

      if (res) {
        dispatch({
          type: 'UPDATE_RECENT_COURSE',
          payload: res.data,
        });
      } else {
        dispatch({
          type: 'UPDATE_RECENT_COURSE',
          payload: [],
        });
      }
    } catch (e) {
      if (e.status === 401) {
        dispatch(expiredAction());
      }
      dispatch({
        type: 'UPDATE_RECENT_COURSE_FAILED',
        payload: e,
      });
    }
  };
}

export function getMostLikeCourse(payload) {
  return async (dispatch) => {
    try {
      dispatch({
        type: 'GETTING_MOST_LIKE_COURSE',
      });
      const res = await API.getMostLike(payload);

      if (res) {
        dispatch({
          type: 'UPDATE_MOST_LIKE_COURSE',
          payload: res.data,
        });
      } else {
        dispatch({
          type: 'UPDATE_MOST_LIKE_COURSE',
          payload: [],
        });
      }
    } catch (e) {
      if (e.status === 401) {
        dispatch(expiredAction());
      }
      dispatch({
        type: 'UPDATE_MOST_LIKE_COURSE_FAILED',
        payload: e,
      });
    }
  };
}

export function getCommentList(payload) {
  return async (dispatch) => {
    try {
      if (payload.params.page === 1) {
        dispatch({
          type: 'GETTING_COMMENT_RESET',
        });
      } else {
        dispatch({
          type: 'GETTING_COMMENT',
        });
      }

      const res = await API.getCommentCourse(payload);

      if (res) {
        dispatch({
          type: 'UPDATE_COMMENT',
          payload: res.data.comments,
          isEnd: false,
        });
      } else {
        dispatch({
          type: 'UPDATE_COMMENT',
          payload: [],
          isEnd: true,
        });
      }
    } catch (e) {
      if (e.status === 401) {
        dispatch(expiredAction());
      }
      if (e.status === 404) {
        dispatch({
          type: 'UPDATE_COMMENT',
          payload: [],
          isEnd: true,
        });
      } else {
        dispatch({
          type: 'UPDATE_COMMENT_FAILED',
          payload: e,
        });
      }
    }
  };
}

export function getSimilarCourse(payload) {
  return async (dispatch) => {
    try {
      dispatch({
        type: 'GETTING_SIMILAR',
      });
      const res = await API.getSimilarCourse(payload);
      if (res) {
        dispatch({
          type: 'UPDATE_SIMILAR',
          payload: res.data.similarCourse,
        });
      } else {
        dispatch({
          type: 'UPDATE_SIMILAR',
          payload: [],
        });
      }
    } catch (e) {
      if (e.status === 401) {
        dispatch(expiredAction());
      }
      if (e.status === 404) {
        dispatch({
          type: 'UPDATE_SIMILAR',
          payload: [],
        });
      } else {
        dispatch({
          type: 'UPDATE_SIMILAR_FAILED',
          payload: e,
        });
      }
    }
  };
}

export function getCourseListPagination(type, payload) {
  return async (dispatch) => {
    try {
      if (payload.params.page === 1) {
        dispatch({
          type: 'GETTING_COURSE_PAGINATION_RESET',
        });
      } else {
        dispatch({
          type: 'GETTING_COURSE_PAGINATION',
        });
      }
      let res = {};
      if (type === 'category') {
        res = await API.getCourseByCategory(payload);
      } else if (type === 'popular') {
        res = await API.getPopular(payload);
      } else if (type === 'new') {
        res = await API.getNewCourse(payload);
      } else if (type === 'mostLike') {
        res = await API.getMostLike(payload);
      }

      if (res) {
        dispatch({
          type: 'UPDATE_COURSE_PAGINATION',
          payload: res.data,
          isEnd: res.data.length < 10 ? true : false,
        });
      } else {
        dispatch({
          type: 'UPDATE_COURSE_PAGINATION',
          payload: [],
          isEnd: true,
        });
      }
    } catch (e) {
      if (e.status === 401) {
        dispatch(expiredAction());
      }
      dispatch({
        type: 'UPDATE_COURSE_PAGINATION_FAILED',
        payload: e,
      });
    }
  };
}

export function getMaterialCourse(payload) {
  return async (dispatch) => {
    try {
      dispatch({
        type: 'GETTING_MATERIAL',
      });
      const res = await API.readMaterial(payload);
      if (res) {
        dispatch({
          type: 'UPDATE_MATERIAL',
          payload: res.data,
        });
      } else {
        dispatch({
          type: 'UPDATE_MATERIAL',
          payload: {},
        });
      }
    } catch (e) {
      if (e.status === 401) {
        dispatch(expiredAction());
      }
      if (e.status === 404) {
        dispatch({
          type: 'UPDATE_MATERIAL',
          payload: {},
        });
      } else {
        dispatch({
          type: 'UPDATE_MATERIAL_FAILED',
          payload: e,
        });
      }
    }
  };
}

export function getExam(payload) {
  return async (dispatch) => {
    try {
      dispatch({
        type: 'GETTING_EXAM',
      });
      const res = await API.getExam(payload);
      if (res) {
        dispatch({
          type: 'UPDATE_EXAM',
          payload: res.data.examQuestions,
        });
      } else {
        dispatch({
          type: 'UPDATE_EXAM',
          payload: [],
        });
      }
    } catch (e) {
      if (e.status === 401) {
        dispatch(expiredAction());
      }
      if (e.status === 404) {
        dispatch({
          type: 'UPDATE_EXAM',
          payload: [],
        });
      } else {
        dispatch({
          type: 'UPDATE_EXAM_FAILED',
          payload: e,
        });
      }
    }
  };
}

export function getMyPreferenceCategory() {
  return async (dispatch) => {
    try {
      dispatch({
        type: 'GETTING_PREFERENCE',
      });
      const res = await API.getUserPreference();

      if (res) {
        dispatch({
          type: 'UPDATE_COURSE_PREFERENCE',
          payload: res.data,
        });
      } else {
        dispatch({
          type: 'UPDATE_COURSE_PREFERENCE',
          payload: [],
        });
      }
    } catch (e) {
      if (e.status === 401) {
        dispatch(expiredAction());
      }
      dispatch({
        type: 'UPDATE_COURSE_PREFERENCE_FAILED',
        payload: e,
      });
    }
  };
}

export function flagCompleteMaterial(payload, courseId) {
  return async (dispatch) => {
    try {
      const res = await API.flagCompleteMaterial(payload);
      console.log(res);
      if (res) {
        dispatch(
          getMaterialCourse({
            paramsId: courseId,
          }),
        );
      }
    } catch (e) {}
  };
}

export function getCourseCheck(payload) {
  return async (dispatch) => {
    try {
      dispatch({
        type: 'GETTING_COURSE_CHECK',
      });

      const res = await API.checkCourseStatus(payload);
      console.log(res);
      if (res) {
        dispatch({
          type: 'UPDATE_COURSE_CHECK',
          payload: res,
        });
      } else {
        dispatch({
          type: 'UPDATE_COURSE_CHECK',
          payload: {
            isCompleted: false,
            isSubscribed: false,
            isExam: false,
            mark: null,
            isReview: false,
          },
        });
      }
    } catch (e) {
      if (e.status === 401) {
        dispatch(expiredAction());
      }
      dispatch({
        type: 'UPDATE_COURSE_CHECK_FAILED',
        payload: e,
      });
    }
  };
}
