import API from 'configs/api';
import { AsyncStorage, showMessage, DeviceInfo } from 'libraries';
import * as RootNavigation from 'configs/routes/RootNavigation';
import Toast from 'react-native-toast-message';

export const RegisterAction = (payload) => {
  console.log('ini payload', payload);
  return async (dispatch) => {
    // POST LOGIN REQUEST
    dispatch({
      type: 'REGISTER_REQUEST',
    });

    try {
      console.log(payload);
      const res = await API.Register(payload);
      //console.log('error')
      console.log(res);
      // CATCH POST LOGIN SUCCESS
      if (res) {
        console.log(res);
        // if
        // console.log('ini token', res.data.token);
        // // await AsyncStorage.setItem('@token', res.data.token);
        dispatch({
          type: 'REGISTER_SUCCESS',
          // payload: {
          //   fullname: res.data.fullname,
          //   username: res.data.username,
          //   email: res.data.email,
          //   error: false,
          // },
        });
        RootNavigation.reset('LoginPage');
      }
    } catch (err) {
      console.log('ini Error', err);
      // CATCH POST LOGIN ERROR
      dispatch({
        type: 'REGISTER_FAILED',
        payload: {
          error: err,
        },
      });
    }
  };
};
import messaging from '@react-native-firebase/messaging';
import { GetProfileAction, resyncProfile } from '../profile/profileActions';

export function expiredAction() {
  return async (dispatch) => {
    // showMessage({
    //   icon: 'auto',
    //   message: 'Sorry',
    //   description: 'Your Session has been expired',
    //   type: 'warning',
    // });
    Toast.show({
      text1: 'Your Session has been expired',
      position: 'bottom',
      type: 'error',
    });
    RootNavigation.reset('LoginPage');
    dispatch({
      type: 'SESSION_EXPIRED',
    });
  };
}

export const LoginAction = (payload) => {
  console.log('payload');
  console.log(payload);
  return async (dispatch) => {
    // POST LOGIN REQUEST
    dispatch({
      type: 'LOGIN_REQUEST',
    });

    try {
      const res = await API.Login(payload);
      console.log(res);
      //console.log(res.status)
      // CATCH POST LOGIN SUCCESS
      if (res) {
        await AsyncStorage.setItem('@token', res.data.token);
        dispatch(storeFirebaseToken());
        dispatch({
          type: 'LOGIN_SUCCESS',
          payload: res.data,
        });
        dispatch(GetProfileAction());
        console.log('masuk');
        console.log(res.data);
        if (res.data.preference) {
          RootNavigation.reset('BottomTab');
        } else if (res.data.profilePicture === null) {
          RootNavigation.reset('AvatarPage');
        } else {
          RootNavigation.reset('PreferredCategoryPage');
        }
      }
    } catch (err) {
      if (err.status === 401) {
        Toast.show({
          text1: err.data.error.message,
          position: 'bottom',
          type: 'error',
        });
      } else {
        Toast.show({
          text1: 'Something Wrong',
          position: 'bottom',
          type: 'error',
        });
      }
      console.log(err);
      //console.log (err)
      // CATCH POST LOGIN ERROR
      dispatch({
        type: 'LOGIN_FAILED',
        payload: {
          error: err,
        },
      });
    }
  };
};

export function checkTokenValidation() {
  return async (dispatch) => {
    try {
      const res = await API.getProfile();
      if (res) {
        dispatch(storeFirebaseToken());
        dispatch(resyncProfile(res));
      }
      console.log('res: ' + JSON.stringify(res));
    } catch (e) {
      console.log('masuk sini bagian error check Token Validation', e);
      if (e.status == 401) {
        dispatch(expiredAction());
      }
    }
  };
}

export function storeFirebaseToken() {
  return async (dispatch) => {
    try {
      const firebaseToken = await messaging().getToken();
      console.log('ini firebase Token', firebaseToken);
      const deviceId = DeviceInfo.getUniqueId();

      const res = await API.storeToken({
        body: {
          deviceId,
          deviceToken: firebaseToken,
        },
      });
    } catch (e) {
      if (e.status === 401) {
        dispatch(expiredAction());
      }
      console.log(e.status);
    }
  };
}

export function sendEmail() {
  return async (dispatch) => {
    try {
      const res = await API.sendEmail();
      if (res) {
        console.log(res);
      }
    } catch (e) {
      if (e.status == 401) {
        dispatch(expiredAction());
      }
      console.log(e.status);
    }
  };
}

export const sendOTP = (payload) => {
  return async (dispatch) => {
    console.log('otp');
    console.log(payload);
    dispatch({
      type: 'LOGIN_REQUEST',
    });
    try {
      const res = await API.verifyEmail(payload);
      console.log('otp');
      console.log(payload);
      if (res) {
        dispatch({
          type: 'VERIFY_OTP',
          payload: {
            isSuccessVerify: true,
            isEmailVerified: true,
          },
        });
        setTimeout(() => {
          RootNavigation.navigate('NewProfilePage');
        }, 2000);

        Toast.show({
          text1: 'Success',
          position: 'bottom',
          type: 'success',
          visibilityTime: 2000,
        });
      }
    } catch (err) {
      if (err.status == 401) {
        dispatch(expiredAction());
      }
      Toast.show({
        text1: 'Wrong code OTP',
        position: 'bottom',
        type: 'error',
      });
      console.log(err);
      dispatch({
        type: 'VERIFY_OTP',
        payload: {
          isSuccessVerify: false,
        },
      });
    }
  };
};

export function setEmailVerified(payload) {
  return async (dispatch) => {
    dispatch({
      type: 'SET_VERIFIED',
      payload: payload,
    });
  };
}
